<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required', 'email', Rule::unique('customers')->ignore(optional($this->customer)->id),
            ],
            'avatar' => 'file|max:350',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Type an email',
            'email.email' => 'Typ a valid email',
            'email.unique' => 'This email is already in use',
            'avatar.max' => 'The image is very large, max 350KB',
        ];
    }
}
