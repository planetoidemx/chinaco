<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasiveTagGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags.*.name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'tags.0.name.required' => 'You must assign a name for the variants',
        ];
    }
}
