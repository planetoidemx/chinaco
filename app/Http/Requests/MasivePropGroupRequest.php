<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasivePropGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'props.*.name' => 'required',
            'props.*.props' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'props.*.name.required' => 'You must assign a name for the main variants',
            'props.*.props.required' => 'Add at least 1 value',
        ];
    }
}
