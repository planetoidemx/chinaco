<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'sometimes|numeric|min:0|required',
            'image' => 'sometimes|file|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'price.required' => 'Type a price',
            'price.numeric' => 'Price must be numeric',
            'image.max' => 'Please check the size of the image, max. 1MB each',
        ];
    }
}
