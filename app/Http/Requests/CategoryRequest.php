<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cover' => 'file|max:1000',
            'title' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cover.max' => 'The main image is very large, max. 1MB',
            'title.required' => 'Type the category name',
        ];
    }
}
