<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'cover' => 'file|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Type the category name',
            'cover.max' => 'The main image is very large, max. 1MB',
        ];
    }
}
