<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'morph_type' => 'required',
            'morph_id' => 'required',
            'cover' => 'file|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Type the name of the product',
            'morph_type.required' => 'Choose a collection or category',
            'morph_id.required' => 'Choose a collection or category',
            'cover.max' => 'Please check the size of the image, max. 1MB each',
        ];
    }
}
