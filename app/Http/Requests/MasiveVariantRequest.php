<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasiveVariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variants.*.price' => 'numeric|min:0|required',
            'variants.*.image' => 'nullable|sometimes|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'variants.*.price.required' => 'Type price for all variants',
            'variants.*.price.min' => 'Price cannot be a negative value',
            'variants.*.price.numeric' => 'Price must have a numeric value',
            'variants.*.image.max' => 'Please check the size of the images, max. 1MB each',
        ];
    }
}
