<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $always = [
            'id' => $this->id,
            'title' => $this->title,
            'cover' => $this->cover,
            'price' => $this->price,
            'link' => $this->link,
            'has_variants' => $this->hasVariants(),
            'has_tags' => $this->hasTags(),
        ];

        if (Route::currentRouteName() === 'collection.products') {
            return $always;
        }

        $others = [
            'slug' => $this->slug,
            'description' => $this->description ? $this->description : '',
            'is_available' => $this->is_available ? 1 : 0,
            'collection_id' => $this->when($this->productable_type == Collection::class, $this->productable_id),
            'collection' => $this->when($this->productable_type == Collection::class, $this->productable),
            'category_id' => $this->when($this->productable_type == Category::class, $this->productable_id),
            'category' => $this->when($this->productable_type == Category::class, $this->productable),
            'productable' => $this->productable,
        ];

        return $always + $others;
    }
}
