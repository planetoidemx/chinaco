<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_color' => $this->is_color,
            'props' => $this->props,
            'edit_mode' => false,
            'edit_create_tag' => false,
            'level' => $this->level,
        ];
    }
}
