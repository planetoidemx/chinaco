<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'color_hex_1' => $this->color_hex_1,
            'color_hex_2' => $this->color_hex_2,
            'title' => $this->title,
            'uuid' => $this->uuid,
            'is_selected' => false,
        ];
    }
}
