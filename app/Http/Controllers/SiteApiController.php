<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\PropGroupResource;
use App\Http\Resources\VariantResource;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Prop;
use App\Models\Variant;
use Illuminate\Http\Request;

class SiteApiController extends Controller
{
    public $producsByCollection = 6;

    public function collectionProducts(Collection $collection = null)
    {
        $products = (is_null($collection) ? Product::availables() : ($collection->hasCategories() ? $collection->productsThroughCategories() : $collection->products())->whereIsAvailable(true))
            ->orderBy('updated_at', 'desc')
            ->paginate($this->producsByCollection);

        return ProductResource::collection($products);
    }

    public function categoryProducts(Category $category)
    {
        $products = $category->products()->whereIsAvailable(true)
            ->orderBy('updated_at', 'desc')
            ->paginate($this->producsByCollection);

        return ProductResource::collection($products);
    }

    public function product(Request $request, Product $product)
    {
        return (new ProductResource($product))->additional([
            'firstAvailableVariant' => $request->v ? $product->variants()->where('is_available', true)->whereSku($request->v)->first() : $product->variants()->where('is_available', true)->first(),
        ]);
    }

    public function props(Product $product)
    {
        return PropGroupResource::collection($product->propGroups);
    }

    public function variant(Request $request, Product $product, Prop $prop1, Prop $prop2 = null)
    {
        $variant = Variant::whereProductId($product->id)
            ->whereFirstProp($prop1->id)
            ->whereSecondProp(optional($prop2)->id)
            ->firstOrFail();

        return new VariantResource($variant);
    }

    public function subscribe(Request $request)
    {
        abort_unless($request->expectsJson(), 404);

        $rules = ['email' => 'required|email'];
        $mesages = ['email.required' => 'Igresa tu email', 'email.email' => 'Tu email parece estar mal escrito'];

        $request->validate($rules, $mesages);

        Customer::firstOrCreate(['email' => $request->email], ['is_agree' => true]);

        return response()->json([
            'status' => true,
            'notification' => 'Listo',
        ], 200);
    }
}
