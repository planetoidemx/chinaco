<?php

namespace App\Http\Controllers;

use App\Asteroide\Support\Localization;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerSessionController extends Controller
{
    public function account(Request $request)
    {
        $customer = $request->user('shop');

        $timeZone = Localization::getUserTimeZone();

        return view('shop.account', compact('customer', 'timeZone'));
    }

    public function show(Customer $customer)
    {
        return response()->json($customer, 200);
    }

    public function updateProfile(CustomerRequest $request, Customer $customer)
    {
        abort_unless($request->expectsJson(), 404);

        $customer->fill($request->except(['password', 'is_agree']));

        if ($request->filled('password')) {
            $customer->password = bcrypt($request->password);
        }

        if ($request->has('is_agree')) {
            $customer->is_agree = (bool) $request->is_agree;
        }

        $customer->save();

        return response()->json([
            'status' => true,
            'notification' => 'Se actualizaron tus datos. ## Your info has been updated.',
        ], 200);
    }
}
