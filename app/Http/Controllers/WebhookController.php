<?php

namespace App\Http\Controllers;

use App\Mail\OrderPaidOxxo;
use App\Mail\OrderReceiptOxxo;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebhookController extends Controller
{
    public $order;

    public $type;

    public function handler(Request $request)
    {
        try {
            $reference = $request->data->object->id;
            $this->type = $request->type;
            $this->order = Order::wherePaymentReference($reference)->first();

            if (! $this->order->paidWithOxxo()) {
                return false;
            }

            switch ($this->type) {
                case 'order.paid':
                    $this->oxxoPaid($request);
                    break;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function oxxoPaid($request)
    {
        if ($request->data->object->payment_status == 'paid') {
            $this->order->paid_at = now();
            $this->order->status = 'paid';
            $this->order->save();

            $this->sendEmails();
        }
    }

    public function sendEmails()
    {
        switch ($this->type) {
            case 'order.paid':
                // To customer
                Mail::to($this->order->customer->email)->send(new OrderPaidOxxo($this->order));

                // To admins
                Mail::to(User::nofityEmails())->send(new OrderReceiptOxxo($this->order));
                break;
        }
    }
}
