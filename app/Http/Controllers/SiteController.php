<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Collection;
use App\Models\Post;
use App\Models\Product;

class SiteController extends Controller
{
    /**
     * Variables --->
     *
     * @return view
     */
    public function home()
    {
        return view('home');
    }

    /**
     * Variables posibles ---> $products, $collection
     *
     * @param  Collection  $collection
     * @return view
     */
    public function shop(Collection $collection = null)
    {
        $collections = Collection::all();
        $collections->load('categories');

        if (request()->category) {
            request()->merge(['category' => Category::whereSlug(request()->category)->first()]);
        }
        // dd($collections);

        return view('shop.store', compact('collections', 'collection'));
    }

    /**
     * Variables posibles ---> $collection, $product
     *
     * @param  Collection  $collection
     * @param  Product  $product
     * @return view
     */
    public function productOfCollection(Collection $collection, Product $product)
    {
        abort_unless($collection->id === $product->productable_id, 404);

        $collections = Collection::all();
        $collections->load('categories');

        if (request()->category) {
            request()->merge(['category' => Category::whereSlug(request()->category)->first()]);
        }

        return view('shop.product', compact('collections', 'collection', 'product'));
    }

    /**
     * Variables posibles ---> $category, $product
     *
     * @param  Collection  $category
     * @param  Product  $product
     * @return view
     */
    public function productOfCategory(Category $category, Product $product)
    {
        abort_unless($category->id === $product->productable_id, 404);

        $collections = Collection::all();
        $collections->load('categories');

        if (request()->category) {
            request()->merge(['category' => Category::whereSlug(request()->category)->first()]);
        }

        return view('shop.product', compact('collections', 'product') + ['collection' => $category]);
    }

    public function blog()
    {
        $posts = Post::forEveryone()->get();
        $lastPost = $posts->first();
        $posts = $posts->filter(fn ($post) => $post->id !== $lastPost->id);

        return view('blog', compact('posts', 'lastPost'));
    }

    public function post(Post $post)
    {
        $posts = Post::forEveryone()->get();

        return view('post', compact('post', 'posts'));
    }
}
