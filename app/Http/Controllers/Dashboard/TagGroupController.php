<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagGroupRequest;
use App\Http\Resources\TagGroupResource;
use App\Models\Product;
use App\Models\TagGroup;

class TagGroupController extends Controller
{
    use Notifications;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return TagGroupResource::collection($product->tagGroups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagGroupRequest $request, Product $product)
    {
        $product->tagGroups()->save(new TagGroup($request->only('name') + ['is_color' => (bool) $request->is_color]));

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagGroup  $tagGroup
     * @return \Illuminate\Http\Response
     */
    public function update(TagGroupRequest $request, Product $product, TagGroup $tagGroup)
    {
        $tagGroup->fill($request->only('name') + ['is_color' => (bool) $request->is_color]);
        $tagGroup->save();

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagGroup  $tagGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, TagGroup $tagGroup)
    {
        $tagGroup->delete();

        return response()->json($this->getApiNotification(), 200);
    }
}
