<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Facades\Monitor;
use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\CollectionRequest;
use App\Models\Collection;

class CollectionController extends Controller
{
    use ControllerHelpers,
        Notifications,
        DealWithFiles;

    public function __construct()
    {
        $this->addNotifications([
            'index' => 'Collection created successfully',
            'update' => 'Your changes have been saved',
            'destroy' => 'A collections has been deleted',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collections = Collection::paginate($this->itemsPerPage);

        return view('admin.collections.items', compact('collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collections = Collection::paginate($this->itemsPerPage);

        return view('admin.collections.create', compact('collections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CollectionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollectionRequest $request)
    {
        $collection = Collection::create($request->all());

        if ($request->hasFile('cover')) {
            $collection->cover_image_path = $this->saveFile("collections/{$collection->id}", $request->cover);
        }

        Monitor::audit(fn () => $collection->save())->addDescription('New collection created');

        $this->prepareNotification($request);

        return redirect()->route('collections.edit', compact('collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $collection)
    {
        $collections = Collection::paginate($this->itemsPerPage);

        return view('admin.collections.edit', compact('collections', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CollectionRequest  $request
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function update(CollectionRequest $request, Collection $collection)
    {
        $collection->fill($request->all());

        if ($request->hasFile('cover')) {
            $this->deleteFile($collection->cover_image_path);
            $collection->cover_image_path = $this->saveFile("collections/{$collection->id}", $request->cover);
        }

        Monitor::model($collection)
            ->audit(fn () => $collection->save())
            ->addDescription('Collection updated');

        $this->prepareNotification($request);

        return redirect()->route('collections.edit', compact('collection'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collection $collection)
    {
        $this->deleteDir("collections/{$collection->id}");

        Monitor::model($collection)
            ->audit(fn () => $collection->delete())
            ->addDescription('Collection modified');

        $this->prepareNotification(request());

        return redirect()->route('collections.index');
    }
}
