<?php

namespace App\Http\Controllers\Dashboard\Traits;

use App\Models\Post;

trait BlogContents
{
    public function belongnsToPost()
    {
        return $this->morphModel('post') instanceof Post;
    }

    public function prepareBlockContent($request)
    {
        switch ($request->layout) {
            case 'text':
            case 'quote':
                return [
                    'text' => $request->text,
                ];

                break;

            case 'image':
                return [
                    'cover_image_path' => $this->saveFile($this->blockableModel()->storagePath('content'), $request->cover),
                    'photo_info' => $request->photo_info,
                ];

                break;

            case 'link':
                return [
                    'link_text' => $request->link_text,
                    'link_button' => $request->link_button,
                    'link' => $request->link,
                ];

                break;

            case 'video':
                return [
                    'video_url' => $request->video_url,
                    'video_description' => $request->video_description,
                ];

            default:
                return [];
        }
    }

    public function updateBlockContent($request, $block)
    {
        switch ($block->layout) {
            case 'text':
            case 'quote':
                return [
                    'text' => $request->text,
                ];

                break;

            case 'image':
                $content = [];

                if ($request->hasFile('cover')) {
                    $this->deleteFile($block->extractContent('cover_image_path'));
                    $content['cover_image_path'] = $this->saveFile($this->blockableModel()->storagePath('content'), $request->cover);
                } else {
                    $content['cover_image_path'] = $block->extractContent('cover_image_path');
                }

                return $content + [
                    'photo_info' => $request->photo_info,
                ];

                break;

            case 'link':
                return [
                    'link_text' => $request->link_text,
                    'link_button' => $request->link_button,
                    'link' => $request->link,
                ];

                break;

            case 'video':
                return [
                    'video_url' => $request->video_url,
                    'video_description' => $request->video_description,
                ];

                break;

            default:
                return [];
        }
    }
}
