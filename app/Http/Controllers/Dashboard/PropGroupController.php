<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropGroupRequest;
use App\Http\Resources\PropGroupResource;
use App\Models\Product;
use App\Models\PropGroup;

class PropGroupController extends Controller
{
    use Notifications,
        ControllerHelpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return PropGroupResource::collection($product->propGroups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropGroupRequest $request, Product $product)
    {
        $propGroup = new PropGroup($request->only('name') + ['is_color' => (bool) $request->is_color]);

        if ($product->propGroups->count() > 0) {
            $propGroup->level = 2;
        }

        $product->propGroups()->save($propGroup);

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropGroup  $propGroup
     * @return \Illuminate\Http\Response
     */
    public function update(PropGroupRequest $request, Product $product, PropGroup $propGroup)
    {
        $propGroup->fill($request->all());
        $propGroup->save();

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropGroup  $propGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, PropGroup $propGroup)
    {
        foreach ($propGroup->props as $prop) {
            foreach ($prop->variants as $variant) {
                $this->deleteFile($variant->cover_image_path);
                $variant->delete();
            }
        }

        $propGroup->delete();

        return response()->json($this->getApiNotification(), 200);
    }
}
