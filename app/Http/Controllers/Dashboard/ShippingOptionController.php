<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShippingOptionRequest;
use App\Models\ShippingOption;

class ShippingOptionController extends Controller
{
    use Notifications, ControllerHelpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shippingOptions = ShippingOption::paginate($this->itemsPerPage);

        return view('admin.shippings.items', compact('shippingOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shippingOptions = ShippingOption::paginate($this->itemsPerPage);

        return view('admin.shippings.create', compact('shippingOptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShippingOptionRequest $request)
    {
        $shippingOption = new ShippingOption($request->all());
        $shippingOption->is_active = (bool) $request->is_active;

        $shippingOption->save();

        return redirect()->route('shippingOptions.edit', compact('shippingOption'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShippingOption  $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function edit(ShippingOption $shippingOption)
    {
        $shippingOptions = ShippingOption::paginate($this->itemsPerPage);

        return view('admin.shippings.edit', compact('shippingOptions', 'shippingOption'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShippingOption  $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function update(ShippingOptionRequest $request, ShippingOption $shippingOption)
    {
        $shippingOption->fill($request->all());
        $shippingOption->is_active = (bool) $request->is_active;
        $shippingOption->save();

        $this->prepareNotification($request);

        return redirect()->route('shippingOptions.edit', compact('shippingOption'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShippingOption  $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShippingOption $shippingOption)
    {
        $shippingOption->delete();

        $this->prepareNotification(request());

        return redirect()->route('shippingOptions.index');
    }
}
