<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\Notifications;
use App\Asteroide\Traits\VariantManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropRequest;
use App\Models\Prop;
use App\Models\PropGroup;
use Illuminate\Http\Request;

class PropController extends Controller
{
    use Notifications,
        VariantManager;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropRequest $request, PropGroup $propGroup)
    {
        $prop = new Prop($request->all());

        $propGroup->props()->save($prop);

        $this->constructNewVariants($propGroup->product, $prop);

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prop  $prop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropGroup $propGroup, Prop $prop)
    {
        $prop->fill($request->all());
        $prop->save();

        $this->reconstructVariantTitles($prop);

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prop  $prop
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropGroup $propGroup, Prop $prop)
    {
        if (! $this->removeOrUnlinkVariants($prop)) {
            return response()->json(['status' => false, 'notification' => "This variant can't be deleted"], 200);
        }

        if ($propGroup->props->count() === 1) {
            $propGroup->delete();
        } else {
            $prop->delete();
        }

        return response()->json($this->getApiNotification(), 200);
    }
}
