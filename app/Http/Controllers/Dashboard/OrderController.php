<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Mail\OrderShipped;
use App\Models\Carrier;
use App\Models\Order;
use App\Models\ShippingOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    use ControllerHelpers,
        Notifications;

    public function __construct()
    {
        $this->addNotifications([
            'update' => 'This order has been updated',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = (filled($q = request()->query('q')) ? Order::search($q) : Order::with('customer')
            ->where('status', '<>', 'created'))
            ->orderBy('created_at', 'desc')->paginate($this->itemsPerPage);

        $orders->load('customer');

        return view('admin.orders.items', compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.orders.edit', [
            'orders' => Order::with('customer')->where('status', '<>', 'created')->orderBy('created_at', 'desc')->paginate($this->itemsPerPage),
            'order' => $order,
            'carriers' => Carrier::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $sendShippingMail = $order->status != 'complete' && $request->status == 'complete';

        $order->fill($request->all());

        if ($request->has('shipping_option')) {
            $order->shippingOption()->associate(ShippingOption::find($request->shipping_option));
        }

        if ($sendShippingMail) {
            Mail::to($order->customer->email)->send(new OrderShipped($order));
        }

        $order->save();

        $this->prepareNotification($request);

        return redirect()->route('orders.edit', compact('order'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        $this->prepareNotification(request());

        return redirect()->route('orders.index');
    }
}
