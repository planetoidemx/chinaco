<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\Notifications;
use App\Asteroide\Traits\VariantManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\MasivePropGroupRequest;
use App\Http\Requests\MasiveTagGroupRequest;
use App\Http\Requests\MasiveVariantRequest;
use App\Models\Product;
use App\Models\Prop;
use App\Models\PropGroup;
use App\Models\Tag;
use App\Models\TagGroup;
use App\Models\Variant;
use Illuminate\Support\Str;

class MasiveController extends Controller
{
    use ControllerHelpers,
        Notifications,
        VariantManager,
        DealWithFiles;

    public function __construct()
    {
        $this->addNotifications([
            'storeProps' => 'Your changes have been saved',
            'storeVariants' => 'Your changes have been saved',
            'storeTags' => 'Your changes have been saved',
        ]);
    }

    /**
     * Masive store prop and groups
     *
     * @param  PropGroupRequest  $request
     * @param  Product  $product
     * @return Response
     */
    public function storeProps(MasivePropGroupRequest $request, Product $product)
    {
        foreach ($request->props as $index => $requestGroup) {
            $propGroup = new PropGroup(['name' => $requestGroup['name'], 'is_color' => (bool) $requestGroup['is_color'], 'level' => $index + 1]);
            $product->propGroups()->save($propGroup);

            foreach ($requestGroup['props'] as $prop) {
                $propGroup->props()->save(new Prop($prop));
            }
        }

        $group = $product->propGroups->where('level', 1)->first();

        if (! empty($group)) {
            foreach ($group->props as $prop) {
                $this->constructNewVariants($product, $prop);
            }
        }

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Masive store of variants
     *
     * @param  VariantRequest  $request
     * @param  Product  $product
     * @return Response
     */
    public function storeVariants(MasiveVariantRequest $request, Product $product)
    {
        foreach ($request->variants as $requestVariant) {
            $variant = new Variant([
                'sku' => Str::upper((Str::random(3).'-'.Str::random(5))),
                'title' => $requestVariant['title'],
                'price' => $requestVariant['price'],
                'is_available' => (bool) $requestVariant['is_available'],
            ]);

            if ((bool) $requestVariant['set_image']) {
                $variant->cover_image_path = $this->saveFile($product->galleryPath(), $requestVariant['image']);
            }

            if (isset($requestVariant['props'])) {
                if (isset($requestVariant['props'][0])) {
                    $variant->firstProp()->associate($product->props()->where('uuid', $requestVariant['props'][0])->first());
                }

                if (isset($requestVariant['props'][1])) {
                    $variant->secondProp()->associate($product->props()->where('uuid', $requestVariant['props'][1])->first());
                }

                if (isset($requestVariant['props'][2])) {
                    $variant->thirdProp()->associate($product->props()->where('uuid', $requestVariant['props'][2])->first());
                }
            }

            $product->variants()->save($variant);
        }

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Masive store of groups and tags
     *
     * @param  TagGroupRequest  $request
     * @param  Product  $product
     * @return Response
     */
    public function storeTags(MasiveTagGroupRequest $request, Product $product)
    {
        foreach ($request->tags as $requestGroup) {
            $tagGroup = new TagGroup(['name' => $requestGroup['name'], 'is_color' => (bool) $requestGroup['is_color']]);
            $product->tagGroups()->save($tagGroup);

            foreach ($requestGroup['tags'] as $tag) {
                $tagGroup->tags()->save(new Tag($tag));
            }
        }

        return response()->json($this->getApiNotification(), 200);
    }
}
