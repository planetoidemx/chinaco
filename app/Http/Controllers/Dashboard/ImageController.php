<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\MapPolymorphicModels;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Resources\ImageResource;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    use DealWithFiles,
        Notifications,
        MapPolymorphicModels;

    public function __construct()
    {
        $this->registerPolymorphicClasses('product');

        $this->addNotifications([
            'store' => 'Galería ha sido actualizada',
            'destroy' => 'Imagen eliminada de la galería',
        ]);
    }

    public function imageableModel()
    {
        return $this->morphModel('product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->polymorphicMapFromRequest($request);

        return ImageResource::collection($this->imageableModel()->images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request)
    {
        $this->polymorphicMapFromRequest($request);

        foreach ($request->images as $uploadedImage) {
            $this->imageableModel()->images()->save(new Image([
                'path' => $this->saveFile($this->imageableModel()->storagePath('gallery'), $uploadedImage),
            ]));
        }

        return response()->json($this->getApiNotification());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $this->deleteFile($image->path);

        $image->delete();

        return response()->json($this->getApiNotification());
    }
}
