<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Facades\Monitor;
use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;

class PostController extends Controller
{
    use ControllerHelpers,
        DealWithFiles,
        Notifications;

    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate($this->itemsPerPage);

        return view('admin.blog.items', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate($this->itemsPerPage);

        return view('admin.blog.create', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = Post::create($request->all());

        $post->fill([
            'main_image_path' => $request->hasFile('main_image') ? $this->saveFile($post->storagePath(), $request->main_image) : null,
            'cover_image_path' => $request->hasFile('cover') ? $this->saveFile($post->storagePath(), $request->cover) : null,
        ])->save();

        $this->prepareNotification($request);

        return redirect()->route('posts.edit', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate($this->itemsPerPage);

        return view('admin.blog.edit', compact('posts', 'post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $post->fill($request->all());

        if ($request->hasFile('main_image')) {
            $this->deleteFile($post->main_image_path);
            $post->main_image_path = $this->saveFile($post->storagePath(), $request->main_image);
        }

        if ($request->hasFile('cover')) {
            $this->deleteFile($post->cover_image_path);
            $post->cover_image_path = $this->saveFile($post->storagePath(), $request->cover);
        }

        Monitor::model($post)
            ->audit(fn () => $post->save())
            ->addDescription('Post updated');

        $this->prepareNotification($request);

        return redirect()->route('posts.edit', compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->deleteDir($post->storagePath());

        Monitor::model($post)
            ->audit(fn () => $post->delete())
            ->addDescription('Post deleted');

        return redirect()->route('posts.index');
    }
}
