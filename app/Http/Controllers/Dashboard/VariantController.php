<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\VariantRequest;
use App\Http\Resources\VariantResource;
use App\Models\Product;
use App\Models\Variant;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    use ControllerHelpers,
        Notifications,
        DealWithFiles;

    public function __construct()
    {
        $this->addNotifications([
            'storeProps' => 'Your changes have been saved',
            'storeVariants' => 'Your changes have been saved',
            'storeTags' => 'Your changes have been saved',
            'destroy' => 'Your changes have been saved',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return VariantResource::collection($product->variants);
    }

    /**
     * Actualozar variante individual
     *
     * @param  Request  $request
     * @param  Product  $product
     * @param  Variant  $variant
     * @return void
     */
    public function update(VariantRequest $request, Product $product, Variant $variant)
    {
        $variant->fill($request->only('price'));

        if ($request->has('is_available')) {
            $variant->is_available = (bool) $request->is_available;
        }

        if ($request->hasFile('image')) {
            $this->deleteFile($variant->cover_image_path);
            $variant->cover_image_path = $this->saveFile($product->galleryPath(), $request->image);
        }

        $variant->save();

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropGroup  $propGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Variant $variant)
    {
        $variant->is_available = false;
        $variant->save();

        return response()->json($this->getApiNotification(), 200);
    }
}
