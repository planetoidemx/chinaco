<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Facades\Monitor;
use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Product;

class ProductController extends Controller
{
    use ControllerHelpers,
        Notifications,
        DealWithFiles;

    protected $morphedClasses;

    protected $productableClass;

    protected $productableModel;

    public function __construct()
    {
        $this->morphedClasses = collect([
            'category' => Category::class,
            'collection' => Collection::class,
        ]);

        $this->productableClass = '';
        $this->productableModel = (object) [];

        $this->notifications = $this->notifications + [
            'update' => 'Your changes have been saved',
            'destroy' => 'A product has been deleted',
        ];
    }

    protected function mapRouteMorphedClasses($type, $id)
    {
        $this->productableClass = $this->morphedClasses->get($type);
        $model = $this->productableClass::find($id);
        $this->productableModel = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = (filled($q = request()->query('q')) ? Product::search($q) : Product::orderBy('updated_at', 'desc'))->paginate($this->itemsPerPage);

        $products->appends(request()->query('q'));

        return ProductResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->mapRouteMorphedClasses($request->morph_type, $request->morph_id);

        $product = new Product($request->all());

        $this->productableModel->products()->save($product);

        if ($request->hasFile('cover')) {
            $product->cover_image_path = $this->saveFile("products/{$product->id}", $request->cover);
        }

        $product->is_available = (bool) $request->is_available;

        $product->save();

        return (new ProductResource($product))->additional($this->getApiNotification());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->mapRouteMorphedClasses($request->morph_type, $request->morph_id);

        $product->fill($request->all());
        $product->is_available = (bool) $request->is_available;

        if ($request->hasFile('cover')) {
            $this->deleteFile($product->cover_image_path);
            $product->cover_image_path = $this->saveFile("products/{$product->id}", $request->cover);
        }

        Monitor::model($product)
            ->audit(fn () => $this->productableModel->products()->save($product))
            ->addDescription('Product updated');

        return (new ProductResource($product))->additional($this->getApiNotification());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->deleteDir("products/{$product->id}");

        foreach ($product->images as $image) {
            $this->deleteFile($image->path);
        }

        Monitor::model($product)
            ->audit(fn () => $product->delete())
            ->addDescription('Product deleted');

        return response()->json($this->getApiNotification(), 200);
    }
}
