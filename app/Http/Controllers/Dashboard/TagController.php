<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use App\Models\TagGroup;

class TagController extends Controller
{
    use Notifications;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request, TagGroup $tagGroup)
    {
        $tagGroup->tags()->save(new Tag($request->all()));

        return response()->json($this->getApiNotification(), 200);
    }

    /**
     * Update an existend user
     *
     * @param  \App\Http\Requests\TagRequest  $request
     * @param  \App\TagGroup  $tagGroup
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, TagGroup $tagGroup, Tag $tag)
    {
        $tag->fill($request->all());
        $tag->save();

        return response()->json($this->getApiNotification(), 200);
    }

    public function destroy(TagGroup $tagGroup, Tag $tag)
    {
        if ($tagGroup->tags->count() === 1) {
            $tagGroup->delete();
        } else {
            $tag->delete();
        }

        return response()->json($this->getApiNotification(), 200);
    }
}
