<?php

namespace App\Http\Controllers\Dashboard;

use App\Asteroide\Facades\Monitor;
use App\Asteroide\Traits\ControllerHelpers;
use App\Asteroide\Traits\DealWithFiles;
use App\Asteroide\Traits\MapPolymorphicModels;
use App\Asteroide\Traits\Notifications;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use MapPolymorphicModels,
        Notifications,
        ControllerHelpers,
        DealWithFiles;

    public function __construct()
    {
        $this->registerPolymorphicClasses('collection');
    }

    public function categorizableModel()
    {
        return $this->morphModel('collection');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->polymorphicMapFromRequest($request);

        return CategoryResource::collection($this->categorizableModel()->categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $this->polymorphicMapFromRequest($request);

        $category = new Category($request->all());

        if ($request->hasFile('cover')) {
            $category->cover_image_path = $this->saveFile('categories', $request->cover);
        }

        Monitor::audit(fn () => $this->categorizableModel()->categories()->save($category))
            ->addDescription('New category created');

        return response()->json($this->getApiNotification());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $this->polymorphicMapFromRequest($request);

        $category->fill($request->all());

        if ($request->hasFile('cover')) {
            $this->deleteFile($category->cover_image_path);
            $category->cover_image_path = $this->saveFile('categories', $request->cover);
        }

        Monitor::model($category)
            ->audit(fn () => $category->save())
            ->addDescription('Category updated');

        return response()->json($this->getApiNotification());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->deleteFile($category->cover_image_path);

        Monitor::model($category)
            ->audit(fn () => $category->delete())
            ->addDescription('Category deleted');

        return response()->json($this->getApiNotification());
    }
}
