<?php

namespace App\Http\Controllers;

use App\Asteroide\Support\CustomerSession;
use App\Asteroide\Traits\Payments;
use App\Mail\OrderPaid;
use App\Mail\OrderReceipt;
use App\Mail\OrderReferenceOxxo;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Setting;
use App\Models\ShippingOption;
use App\Models\Tag;
use App\Models\User;
use App\Models\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    use Payments;

    public $incompleStatus;

    public $incompleteOrder;

    public $payPalReturnUrl;

    public $payPalCancelUrl;

    public function __construct()
    {
        $this->paymentError = '';
        $this->currency = 'mxn';
        $this->incompleStatus = collect([
            'created', 'waiting_paypal', 'error_paypal', 'error_card',
        ]);
    }

    public function fillOrder()
    {
        $this->order->payment_method = $this->payment->method;
        $this->order->currency_code = $this->currency;

        if ($this->payment->method == 'card') {
            $this->order->payment_holders_name = $this->payment->cardHoldersName;
            $this->order->card_last4 = $this->payment->cardlastFour;
            $this->order->card_brand = $this->payment->cardType;
        }

        $this->order->customer()->associate($this->customer);
        $this->order->shippingOption()->associate($this->shippingOption->id);
    }

    public function resolveShippingFee()
    {
        $shippingSetting = Setting::shipping()->first();

        if ((bool) $shippingSetting->value('is_active') && $this->order->subtotal >= (float) $shippingSetting->value('min_amount')) {
            return 0;
        }

        return $this->shippingOption->fee;
    }

    public function resoleveChargedPrice($productInCart)
    {
        $product = Product::find($productInCart->id);

        if (filled(optional($productInCart)->variant)) {
            $variant = Variant::find($productInCart->variant->id);

            return ($product->has_discount ? $variant->applyDiscount() : $variant->price) * $productInCart->quantity;
        }

        return ($product->has_discount ? $product->applyDiscount() : $product->price) * $productInCart->quantity;
    }

    public function calculateOrder()
    {
        $this->order->amount = collect($this->cart)->sum(function ($productInCart) {
            return $this->resoleveChargedPrice($productInCart);
        });

        $this->order->subtotal = number_format($this->order->amount, 2);
        $this->order->shipping_fee = $this->resolveShippingFee();
        $this->order->discount = 0;
        $this->order->amount = number_format($this->order->subtotal - $this->order->discount + $this->order->shipping_fee, 2);

        $this->order->save();
    }

    public function applyCoupon()
    {
        // Pendiente
    }

    public function syncProducts()
    {
        $this->order->products()->detach();
        foreach ($this->cart as $productInCart) {
            $product = Product::find($productInCart->id);
            $variant = filled(optional($productInCart)->variant) ? Variant::find($productInCart->variant->id) : null;

            // Cuando existan descuentos modificar aquí
            $quantity = $productInCart->quantity;
            $chargedPrice = (filled($variant) ? $variant->price : $product->price) * $quantity;
            $tags = [];

            foreach (optional($productInCart)->tags ?? [] as $tagInCart) {
                $tag = Tag::find($tagInCart->id);

                if (filled($tag)) {
                    array_push($tags, ['group_title' => $tag->tagGroup->name] + $tag->toArray());
                }
            }

            $fields = [
                'quantity' => $quantity,
                'charged_price' => $chargedPrice,
                'product_at_moment' => $product->toArray(),
                'variant_at_moment' => optional($variant)->toArray() ?? [],
                'tags' => $tags,
                'details' => optional($productInCart)->notes,
            ];

            $this->order->products()->attach($product->id, $fields);
        }
    }

    public function openOrder()
    {
        $this->order = filled($this->incompleteOrder) && $this->incompleStatus->contains($this->incompleteOrder->status) ? $this->incompleteOrder : Order::create(['reference' => Str::sku(env('APP_NAME').(string) Str::uuid())]);
    }

    public function setCustomer($request)
    {
        $customer = json_decode($request->customer);
        $extraInfo = json_decode($request->extra);

        $this->customer = Customer::firstOrCreate(['email' => $customer->email], ['first_name' => $customer->first_name]);
        $this->customer->fill((array) $customer + ['extra_info' => (array) $extraInfo]);
        $this->customer->save();

        // if ($customer->register && filled($customer->password) && !auth('shop')->check()) {
        // 	CustomerSession::configure($customer->password, $this->customer)->login();
        // }
    }

    public function setShippingOption($request)
    {
        $this->shippingOption = ShippingOption::find((json_decode($request->shippingOption))->id);
    }

    public function orderIsComplete($reference, $abort = false)
    {
        $this->incompleteOrder = Order::whereReference($reference)->first();

        if ($abort) {
            abort_unless(filled($this->incompleteOrder), 404);
        }

        return filled($this->incompleteOrder) && ! $this->incompleStatus->contains($this->incompleteOrder->status);
    }

    protected function sendMailNotifications()
    {
        try {
            Mail::to(User::notifyEmails())->send(new OrderPaid($this->order));
            Mail::to($this->order->customer->email)->send(new OrderReceipt($this->order));

            if ($this->order->paidWithOxxo()) {
                Mail::to($this->order->customer->email)->send(new OrderReferenceOxxo($this->order));
            }
        } catch (\Throwable $th) {
            // Mail Error
        }
    }

    public function pay(Request $request)
    {
        if ($this->orderIsComplete($request->query('order'))) {
            return view('shop.receipt', ['order' => $this->incompleteOrder]);
        }

        if (session('wait_for_paypal') || $request->payerID) {
            $this->restoreSession();
            $this->openOrder();
            $this->order->fill($this->completePayPalPayment() ?? []);
            $this->order->save();

            if ($this->order->status === 'paid') {
                $this->sendMailNotifications();
            }

            $this->forgetSessionItems();
        } else {
            if (! $request->has('cart') || ! $request->has('customer') || ! $request->has('payment')) {
                return redirect()->route('shop.cart');
            }

            $this->cart = json_decode($request->cart);
            $this->payment = json_decode($request->payment);

            $this->setCustomer($request);
            $this->setShippingOption($request);
            $this->openOrder();
            $this->fillOrder();
            $this->calculateOrder();
            $this->syncProducts();

            switch ($this->payment->method) {
                case 'card':
                    $charge = $this->payWithCard($request);

                    if (! $this->anyError()) {
                        $this->order->fill($charge);
                        $this->order->save();
                        $this->sendMailNotifications();
                    }

                    break;

                case 'paypal':

                    $this->payPalReturnUrl = route('shop.process', ['order' => $this->order->reference]);
                    $this->payPalCancelUrl = route('shop.checkout', ['order' => $this->order->reference]);

                    $this->order->fill($this->payWithPayPal($request) ?? []);
                    $this->order->save();

                    if (! $this->anyError()) {
                        $this->saveSession();

                        return redirect()->away($this->order->approval_link);
                    }

                    break;

                case 'oxxo_cash':

                    $this->order->fill($this->payWithOxxoChash($request) ?? []);
                    $this->order->save();

                    if (! $this->anyError()) {
                        $this->sendMailNotifications();
                    }

                    break;
            }
        }

        return redirect(route('shop.order', ['order' => $this->order->reference]))->with('paymentError', $this->paymentError);
    }

    public function order(Request $request, $order)
    {
        return view($this->orderIsComplete($order, true) ? 'shop.receipt' : 'shop.error', [
            'order' => $this->incompleteOrder,
            'error' => session('paymentError'),
        ]);
    }
}
