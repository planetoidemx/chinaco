<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Asteroide\Traits\SlugRouting;
use App\Models\Accessors\ProductAccessors;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use SlugRouting,
        Searchable,
        FileUrls,
        SoftDeletes,
        ProductAccessors;

    protected $fillable = [
        'title',
        'description',
        'cover_image_path',
        'price',
        'is_available',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'cover_image_path',
    ];

    protected $appends = [
        'cover',
        'link',
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    #[SearchUsingFullText(['description'])]
    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
        ];
    }

    public function productable()
    {
        return $this->morphTo();
    }

    public function propGroups()
    {
        return $this->hasMany(PropGroup::class);
    }

    public function tagGroups()
    {
        return $this->hasMany(TagGroup::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)
            ->using(OrderProduct::class)
            ->withPivot([
                'quantity',
                'charged_price',
                'product_at_moment',
                'variant_at_moment',
            ])->withTimestamps();
    }

    public function scopeAvailables($query)
    {
        return $query->where('is_available', true);
    }

    public function props()
    {
        return $this->hasManyThrough(Prop::class, PropGroup::class);
    }

    public function tags()
    {
        return $this->hasManyThrough(Tag::class, TagGroup::class);
    }

    public function variants()
    {
        return $this->hasMany(Variant::class);
    }

    public function hasVariants()
    {
        return $this->variants->count() > 0;
    }

    public function hasTags()
    {
        return $this->tags->count() > 0;
    }

    public function getRelated()
    {
        return self::where('id', '<>', $this->id)
            ->where('productable_id', $this->productable_id)
            ->where('productable_type', $this->productable_type)
            ->get()
            ->take(3);
    }

    public function gallery()
    {
        $gallery = [];

        if (filled($this->cover_image_path)) {
            array_push($gallery, ['id' => 'PROD-COVER', 'url' => $this->cover]);
        }

        foreach ($this->images as $image) {
            array_push($gallery, [
                'id' => 'PROD-'.$image->id,
                'url' => $image->url,
            ]);
        }

        return array_merge($gallery, $this->variantGallery()->all());
    }

    public function variantGallery()
    {
        return $this->variants->map(function ($variant) {
            return [
                'id' => $variant->sku,
                'url' => $variant->cover,
            ];
        })->filter(function ($variant) {
            return $variant['url'];
        });
    }

    public function galleryPath()
    {
        return "products/{$this->id}/variants";
    }

    public function storagePath()
    {
        return "products/{$this->id}";
    }

    public function loadedVariants()
    {
        $this->variants->map(function ($variant) {
            $variant->loadProps();
        });

        return $this->variants;
    }

    public function loadedTagGroups()
    {
        $this->tagGroups->map(function ($tagGroup) {
            $tagGroup->loadTags();
        });

        return $this->tagGroups;
    }

    public function applyDiscount()
    {
        return null;
    }

    public function getApplyDiscountAttribute()
    {
        return $this->applyDiscount();
    }

    public function asResource()
    {
        return new \App\Http\Resources\ProductResource($this);
    }
}
