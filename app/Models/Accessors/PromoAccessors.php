<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait PromoAccessors
{
    /**
     * Get the promo's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function cover(): Attribute
    {
        return new Attribute(
            get: fn () => $this->fileUrl('cover_image_path'),
        );
    }

    /**
     * Get the promo's coverRx.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function coverRx(): Attribute
    {
        return new Attribute(
            get: fn () => $this->fileUrl('cover_rx_image_path'),
        );
    }
}
