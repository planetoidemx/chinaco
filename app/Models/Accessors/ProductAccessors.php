<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait ProductAccessors
{
    /**
     * Get the product's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function cover(): Attribute
    {
        return new Attribute(
            get: fn () => $this->fileUrl('cover_image_path'),
        );
    }

    /**
     * Get the product's link.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function link(): Attribute
    {
        $morphType = Str::contains($this->productable_type, 'Collection') ? 'collection' : 'category';
        $routeName = "shop.product.of.$morphType";

        return new Attribute(
            get: fn ($value, $attributes) => route($routeName, [
                $morphType => $this->productable->slug,
                'product' => $this->slug,
            ]),
        );
    }
}
