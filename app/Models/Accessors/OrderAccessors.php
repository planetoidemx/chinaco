<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait OrderAccessors
{
    /**
     * Get the order's payment holder's name.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function paymentHoldersName(): Attribute
    {
        return new Attribute(
            get: fn ($value) => strtoupper($value),
        );
    }

    /**
     * Get the order's link.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function link(): Attribute
    {
        return new Attribute(
            get: fn () => route('shop.order', ['order' => $this->reference]),
        );
    }

    /**
     * Get the order's OXXO reference.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function oxxoReference(): Attribute
    {
        return new Attribute(
            get: fn () => $this->approval_link,
        );
    }

    /**
     * Get the order's payment error message.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function paymentErrorMessage(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => function ($attributes) {
                switch ($attributes['status']) {
                    case 'error_paypal':
                    case 'error_card':
                        return 'Payment declined';
                        break;

                    case 'waiting_paypal':
                    return 'Wating for PayPal authorization';
                    break;

                    default:
                        return ucfirst($this->attributes['status']);
                        break;
                }
            },
        );
    }

    /**
     * Get the order's tracking url.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function trackingUrl(): Attribute
    {
        return new Attribute(
            get: fn () => $this->tracking_link.$this->track_number,
        );
    }
}
