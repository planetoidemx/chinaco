<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait CollectionAccessors
{
    /**
     * Get the collection's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function cover(): Attribute
    {
        return new Attribute(
            get: fn () => $this->fileUrl('cover_image_path'),
        );
    }

    /**
     * Get the collection's link.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function link(): Attribute
    {
        return new Attribute(
            get: fn () => route('shop.index', [
                'collection' => $this->slug,
            ]),
        );
    }
}
