<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait VariantAccessors
{
    /**
     * Get the variant's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function cover(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => $this->fileUrl('cover_image_path'),
        );
    }
}
