<?php

namespace App\Models\Accessors;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait CategoryAccessors
{
    /**
     * Get the category's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function cover(): Attribute
    {
        return new Attribute(
            get: fn () => $this->fileUrl('cover_image_path'),
        );
    }

    /**
     * Get the category's cover.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function link(): Attribute
    {
        return new Attribute(
            get: fn () => Str::contains($this->categorizable_type, 'Collection') ? optional($this->categorizable)->link($this->slug) : '',
        );
    }
}
