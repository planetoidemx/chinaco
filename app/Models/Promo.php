<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Models\Accessors\PromoAccessors;
use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    use FileUrls,
        PromoAccessors;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'cover_image_path',
        'cover_rx_image_path',
        'link',
    ];

    protected $appends = [
        'cover',
        'cover_rx',
    ];

    protected $hidden = [
        'cover_image_path',
        'cover_rx_image_path',
    ];
}
