<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropGroup extends Model
{
    protected $fillable = [
        'name',
        'is_color',
        'level',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function props()
    {
        return $this->hasMany(Prop::class);
    }
}
