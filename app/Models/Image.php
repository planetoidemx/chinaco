<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Models\Accessors\ImageAccessors;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use FileUrls,
        ImageAccessors;

    protected $fillable = [
        'path',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'path',
    ];

    protected $appends = [
        'url',
    ];

    public function imageable()
    {
        return $this->morphTo();
    }
}
