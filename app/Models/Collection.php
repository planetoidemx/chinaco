<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Asteroide\Traits\SlugRouting;
use App\Models\Accessors\CollectionAccessors;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use FileUrls,
        SlugRouting,
        CollectionAccessors;

    protected $fillable = [
        'title',
        'description',
        'cover_image_path',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'cover',
        // 'link',
    ];

    /**
     * Get all of the products for the collection
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function products()
    {
        return $this->morphMany(Product::class, 'productable');
    }

    public function productsThroughCategories()
    {
        return $this->hasManyThrough(Product::class, Category::class, 'categorizable_id', 'productable_id')->where('productable_type', 'App\Models\Category');
    }

    /**
     * Get all of the blocks for the collection
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function blocks()
    {
        return $this->morphMany(Block::class, 'blockable');
    }

    /**
     * Get all of the categories for the collection
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function categories()
    {
        return $this->morphMany(Category::class, 'categorizable');
    }

    public function hasCategories()
    {
        return $this->categories->count() > 0;
    }
}
