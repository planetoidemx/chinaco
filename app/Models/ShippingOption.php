<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingOption extends Model
{
    protected $fillable = [
        'name',
        'tracking_link',
        'description',
        'fee',
        'is_active',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'tracking_link',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
