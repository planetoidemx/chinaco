<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Models\Accessors\VariantAccessors;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    use FileUrls,
        VariantAccessors;

    protected $fillable = [
        'title',
        'price',
        'cover_image_path',
        'is_available',
        'sku',
    ];

    protected $appends = [
        'cover',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'third_prop',
        'deleted_at',
        'cover_image_path',
        'product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function firstProp()
    {
        return $this->belongsTo(Prop::class, 'first_prop');
    }

    public function secondProp()
    {
        return $this->belongsTo(Prop::class, 'second_prop');
    }

    public function loadProps()
    {
        $this->load('firstProp');
        $this->load('secondProp');

        return $this;
    }

    public function applyDiscount()
    {
        return null;
    }

    public function getApplyDiscountAttribute()
    {
        return $this->applyDiscount();
    }
}
