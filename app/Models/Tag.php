<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'title',
        'color_hex_1',
        'color_hex_2',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'tag_group_id',
    ];

    public function tagGroup()
    {
        return $this->belongsTo(TagGroup::class);
    }
}
