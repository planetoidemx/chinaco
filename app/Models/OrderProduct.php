<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProduct extends Pivot
{
    public $incrementing = true;

    protected $casts = [
        'product_at_moment' => 'object',
        'variant_at_moment' => 'object',
        'tags' => 'object',
    ];

    protected $fillable = [
        'quantity',
        'charged_price',
        'product_at_moment',
        'variant_at_moment',
        'tags',
        'details',
    ];

    public function hasVariant()
    {
        return filled($this->variant_at_moment);
    }

    public function variant($prop)
    {
        return optional($this->variant_at_moment)->{$prop};
    }

    public function tagsAsString()
    {
        $tags = '';

        foreach ($this->tags ?? [] as $tag) {
            $tags .= $tag->group_title.': '.$tag->title.' | ';
        }

        return trim(trim(trim($tags), '|'));
    }
}
