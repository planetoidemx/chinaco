<?php

namespace App\Models;

use App\Models\Accessors\CustomerAccessors;
use App\Notifications\CustomerResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;

class Customer extends Authenticatable
{
    use Searchable,
        Notifiable,
        CustomerAccessors;

    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'postal_code',
        'city',
        'state',
        'country',
        'references',
        'phone',
        'email',
        'extra_info',
        'password',
        'is_agree',
    ];

    protected $casts = [
        'extra_info' => 'object',
    ];

    protected $hidden = [
        'password',
        'api_token',
        'remember_token',
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return $this->toArray() + [
            // Customize the data array...
        ];
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function billingAddress($prop)
    {
        return optional(optional($this->extra_info)->billingAddress)->{$prop};
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function scopeForNewsletter($query)
    {
        return $query->where('is_agree', true);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token, request()->lang));
    }
}
