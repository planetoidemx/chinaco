<?php

namespace App\Models;

use App\Asteroide\Traits\FileUrls;
use App\Asteroide\Traits\SlugRouting;
use App\Models\Accessors\CategoryAccessors;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use FileUrls,
        SlugRouting,
        CategoryAccessors;

    protected $fillable = [
        'title',
        'cover_image_path',
        'description',
    ];

    protected $appends = [
        'link',
    ];

    public function categorizable()
    {
        return $this->morphTo();
    }

    /**
     * Get all of the products for the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function products()
    {
        return $this->morphMany(Product::class, 'productable');
    }
}
