<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagGroup extends Model
{
    protected $fillable = [
        'name',
        'is_color',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function tags()
    {
        return $this->hasMany(Tag::class);
    }

    public function loadTags()
    {
        $this->load('tags');
    }
}
