<?php

namespace App\Models;

use App\Models\Accessors\OrderAccessors;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Order extends Model
{
    use Searchable,
        OrderAccessors;

    /**
     * status -> [created, waiting_paypal, waiting_oxxo, error_paypal, error_card, error_oxxo, expired_oxxo, paid,  complete]
     * payment_method -> [card, paypal, oxxo_cash]
     *
     * @var array
     */
    protected $fillable = [
        'reference',
        'status',
        'payment_method',
        'payment_reference',
        'payment_holders_name',
        'card_brand',
        'card_last4',
        'receipt_url',
        'error_message',
        'approval_link',
        'paid_at',
        'currency_code',
        'amount',
        'shipping_fee',
        'subtotal',
        'discount',
        'track_number',
        'expires_at',
        'carrier_name',
        'tracking_link',
    ];

    protected $dates = [
        'paid_at',
        'created_at',
        'updated_at',
        'expires_at',
    ];

    // protected $with = [
    // 	'customer'
    // ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shippingOption()
    {
        return $this->belongsTo(ShippingOption::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->using(OrderProduct::class)
            ->withPivot([
                'quantity',
                'charged_price',
                'product_at_moment',
                'variant_at_moment',
            ])->withTimestamps();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return $this->toArray() + [
            // 'customer' => $this->customer
        ];
    }

    public function paidWithCard()
    {
        return $this->payment_method === 'card';
    }

    public function paidWithPayPal()
    {
        return $this->payment_method === 'paypal';
    }

    public function paidWithOxxo()
    {
        return $this->payment_method === 'oxxo_cash';
    }

    public function hasPaymentError()
    {
        return $this->status === 'error_paypal' || $this->status === 'error_card';
    }
}
