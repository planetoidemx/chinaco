<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prop extends Model
{
    protected $fillable = [
        'title',
        'color_hex_1',
        'color_hex_2',
        'uuid',
    ];

    public function propGroup()
    {
        return $this->belongsTo(PropGroup::class);
    }

    public function firstVariants()
    {
        return $this->hasMany(Variant::class, 'first_prop');
    }

    public function secondVariants()
    {
        return $this->hasMany(Variant::class, 'second_prop');
    }

    public function variants()
    {
        return $this->firstvariants->merge($this->secondVariants->all());
    }

    public function getVariantsAttribute()
    {
        return $this->variants();
    }
}
