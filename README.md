# Asteroide

Construido con Laravel 9.\* y Vue 3

Instalación. Después de clonar ejecutar:

1. composer i && composer run-script asteroide-init
1. php artisan ziggy:generate
1. npm ci --save && npm run dev

Reset DB:

1. php artisan migrate:refresh --seed
