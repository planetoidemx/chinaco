<?php

namespace Database\Seeders;

use App\Models\Collection;
use Illuminate\Database\Seeder;

class CollectionsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            //
        ];

        foreach ($collections as $collection) {
            Collection::create($collection);
        }

        $this->command->warn('Collections created!');
    }
}
