<?php

namespace Database\Seeders;

use App\Models\Carrier;
use Illuminate\Database\Seeder;

class CarrierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrieres = [
            [
                'name' => 'UPS',
                'tracking_link' => 'http://wwwapps.ups.com/WebTracking/processInputRequest?TypeOfInquiryNumber=T&InquiryNumber1=%s',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'FedEx',
                'tracking_link' => 'https://www.fedex.com/fedextrack/?tracknumbers=%s',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'AMPM',
                'tracking_link' => 'http://www.grupoampm.com/rastreo/',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'DHL',
                'tracking_link' => 'http://www.dhl.com.mx/en/express/tracking.html?AWB=%s&brand=DHL',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'REDPACK',
                'tracking_link' => 'https://www.redpack.com.mx/rastreo-de-envios',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'ENVIA',
                'tracking_link' => 'http://www.enviageb.com.mx:8080/pages/detalleNuevoRastreo.jsp?arrayGuias=%s',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Estafeta',
                'tracking_link' => 'https://www.estafeta.com/Herramientas/Rastreo/',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'PAQUETEXPRESS',
                'tracking_link' => 'https://www.paquetexpress.com.mx/',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'SendEx',
                'tracking_link' => 'http://www.sendex.mx/Rastreo/Rastreo/',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'qüiken',
                'tracking_link' => 'http://quiken.mx/rastreo?num=%s',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        Carrier::insert($carrieres);

        $this->command->warn('Paqueterías agregadas');
    }
}
