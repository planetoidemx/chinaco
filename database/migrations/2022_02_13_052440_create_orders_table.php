<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');
            $table->enum('status', [
                'created',
                'waiting_paypal',
                'waiting_oxxo',
                'error_paypal',
                'error_card',
                'error_oxxo',
                'expired_oxxo',
                'paid',
                'complete',
            ])->default('created');
            $table->enum('payment_method', [
                'card',
                'paypal',
                'oxxo_cash',
            ])->default('card');
            $table->string('payment_reference')->nullable();
            $table->string('payment_holders_name')->nullable();
            $table->string('card_brand', 100)->nullable();
            $table->string('card_last4', 10)->nullable();
            $table->string('receipt_url')->nullable();
            $table->longText('error_message')->nullable();
            $table->string('approval_link')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->string('currency_code', 10)->nullable();
            $table->unsignedFloat('amount')->default(0);
            $table->unsignedFloat('shipping_fee')->default(0);
            $table->unsignedFloat('subtotal')->default(0);
            $table->unsignedFloat('discount')->default(0);
            $table->string('track_number')->nullable();
            $table->unsignedBigInteger('shipping_option_id')->nullable();
            $table->foreign('shipping_option_id')
                ->references('id')->on('shipping_options')
                ->onDelete('set null');
            $table->string('carrier_name')->nullable();
            $table->string('tracking_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
