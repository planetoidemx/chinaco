<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->string('sku')->nullable();
            $table->string('title')->nullable();
            $table->unsignedFloat('price')->default(0);
            $table->string('cover_image_path')->nullable();
            $table->boolean('is_available')->default(true);
            $table->unsignedBigInteger('first_prop')->nullable();
            $table->foreign('first_prop')
                ->references('id')->on('props')
                ->onDelete('set null');
            $table->unsignedBigInteger('second_prop')->nullable();
            $table->foreign('second_prop')
                ->references('id')->on('props')
                ->onDelete('set null');
            $table->unsignedBigInteger('third_prop')->nullable();
            $table->foreign('third_prop')
                ->references('id')->on('props')
                ->onDelete('set null');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
};
