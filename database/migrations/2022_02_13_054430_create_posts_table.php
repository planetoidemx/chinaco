<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('title');
            $table->string('type', 100)->default('post');
            $table->string('source_link')->nullable();
            $table->string('custom_date')->nullable();
            $table->string('author')->nullable();
            $table->string('link')->nullable();
            $table->string('main_image_path')->nullable();
            $table->string('cover_image_path')->nullable();
            $table->string('visibility', 100)->default('everyone');
            $table->nullableMorphs('postable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
