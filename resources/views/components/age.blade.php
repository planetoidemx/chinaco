<div id="age" :class="{active:!legalage || !choosedlang}">
	<div class="blurfoto" style="background: url('/images/cover1.jpg')no-repeat center center;"></div>
	<div class="blurcolor"></div>
	<div class="centro" v-if="!askedage && !legalage">
		<img src="/images/chinaco.svg">
		<div class="form">
			<span>{{ __('¿Eres mayor de edad?') }}</span>
			<div class="actions">
				<button class="boton" @click="confirmlegalage"><span>{{ __('SÍ') }}</span></button>
				<button class="boton" @click="underage"><span>NO</span></button>
			</div>
		</div>
	</div>
	<div class="centro" v-if="legalage && !choosedlang">
		<img src="/images/chinaco.svg">
		<div class="form">
			<div class="actions">
				<button class="boton"  @click="setlang('es')"><span>Español</span></button>
				<button class="boton"  @click="setlang('en')"><span>English</span></button>
			</div>
		</div>
	</div>
	<div class="msg" v-if="!legalage && askedage">
		<span>{{ __('Debes ser mayor de edad para ingresar al sitio') }}</span>
		<button class="boton" @click="askagain"><span>{{ __('Aceptar') }}</span></button>
	</div>
</div>