<footer>
	<div class="centrador">
		<div class="rrss">
			<a href="https://www.facebook.com/TequilaChinacoPremium" target="_tab"><div class="ico"><i class="fa fa-facebook-f" aria-hidden="true"></i></div></a>
			<a href="https://www.instagram.com/chinaco_tequila/" target="_tab"><div class="ico"><i class="fa fa-instagram" aria-hidden="true"></i></div></a>
		</div>
		<small>{{ __('Por favor disfrute responsablemente. Tequila Chinaco @ 40% alc/vol (80 grados). Importado por Hotaling & Co, San Francisco, CA.') }} &copy; 2022</small>
		<div id="scroll" v-scroll-to="'#header'">
			<img src="/images/scroll.svg">
		</div>
	</div>
</footer>