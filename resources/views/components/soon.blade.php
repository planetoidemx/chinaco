<div id="soon">
	<div id="photos">
		<div class="photo desktop active" style="background: url('/images/cover1.jpg')no-repeat center center;"></div>
		<div class="photo desktop" style="background: url('/images/cover2.jpg')no-repeat center center;"></div>
		<div class="photo desktop" style="background: url('/images/cover3.jpg')no-repeat center center;"></div>
		<div class="photo desktop" style="background: url('/images/cover4.jpg')no-repeat center center;"></div>
		<div class="photo mobile active" style="background: url('/images/cover1_m.jpg')no-repeat center center;"></div>
		<div class="photo mobile" style="background: url('/images/cover2_m.jpg')no-repeat center center;"></div>
		<div class="photo mobile" style="background: url('/images/cover3_m.jpg')no-repeat center center;"></div>
		<div class="photo mobile" style="background: url('/images/cover4_m.jpg')no-repeat center center;"></div>
	</div>
	<div class="centrador">
		<div class="row" id="top">
			<img src="/images/chinaco.svg" id="logo">
			<img src="/images/tagline.svg" id="tagline">
		</div>
		<div class="row" id="bottom">
			<img src="/images/50years.svg" id="years">
			<div id="social">
				<img src="/images/new.svg" id="new">
				<div class="social">
					<div class="rrss">
						<a href="https://www.facebook.com/TequilaChinacoPremium"><div class="ico"><i class="fa fa-facebook-f" aria-hidden="true"></i></div></a>
					</div>
					<div class="rrss">
						<a href="https://www.instagram.com/chinaco_tequila/"><div class="ico"><i class="fa fa-instagram" aria-hidden="true"></i></div></a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>