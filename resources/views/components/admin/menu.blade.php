<dashboard-menu :items="{{
	json_encode(array_merge([

	[
		'active' => Str::contains(Request::path(), 'users'),
		'link' => route('users.index'),
		'icon' => 'icon-user',
		'title' => auth('admin')->user()->hasRole('regular') ? 'PROFILE' : 'USERS'
	],

	[
		'active' => Str::contains(Request::path(), 'promos'),
		'link' => route('promos.index'),
		'icon' => 'icon-star',
		'title' => 'PROMOS'
	],

	[
		'active' => Str::contains(Request::path(), 'collections'),
		'link' => route('collections.index'),
		'icon' => 'icon-diamond',
		'title' => 'COLLECTIONS'
	],

	[
		'active' => Str::contains(Request::path(), 'product'),
		'link' => route('spa.products'),
		'icon' => 'icon-shop',
		'title' => 'SHOP'
	],

	[
		'active' => Str::contains(Request::path(), 'shippings'),
		'link' => route('shippingOptions.index'),
		'icon' => 'icon-truck',
		'title' => 'SHIPPING'
	],

	[
		'active' => Str::contains(Request::path(), 'discounts'),
		'link' => route('discounts.index'),
		'icon' => 'icon-tag',
		'title' => 'DISCOUNTS'
	],

	[
		'active' => Str::contains(Request::path(), 'orders'),
		'link' => route('orders.index'),
		'icon' => 'icon-inbox',
		'title' => 'ORDERS'
	],

	[
		'active' => Str::contains(Request::path(), 'site'),
		'link' => route('website.index'),
		'icon' => 'icon-desktop',
		'title' => 'SITE'
	],

	[
		'active' => Str::contains(Request::path(), 'blog'),
		'link' => route('posts.index'),
		'icon' => 'icon-doc',
		'title' => 'BLOG'
	],

	], auth('admin')->user()->hasAdminRoleLevel() ? [
		[
			'active' => Str::contains(Request::path(), 'settings'),
			'link' => route('settings.index'),
			'icon' => 'icon-params',
			'title' => 'AJUSTES'
		],
	] : []
	, auth('admin')->user()->hasRole('super_admin') ? [[
		'active' => Str::contains(Request::path(), 'monitor'),
		'link' => route('monitor.index'),
		'icon' => 'icon-eye',
		'title' => 'MONITOR'
	]] : [])) }}">

	<template v-slot:csrf>
		<form action="{{ route('admin.logout') }}" id="logout-form-component" method="POST">
			@csrf
		</form>
	</template>

</dashboard-menu>
