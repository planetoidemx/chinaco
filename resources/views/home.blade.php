@extends('layouts.default')

@section('title','Chinaco®')

@section('metas')

   	<meta property="og:url" content="{{ Request::url() }}"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Chinaco®"/>
	<meta property="og:image" content="{{ asset('images/meta_img.jpg') }}"/>
	<meta property="og:description" content="Tequila Excepcional"/>
	<meta property="fb:app_id" content=""/>

@endsection


@section('content')


	<div id="site">
		
		
		@include('components.age')
		@include('components.header')
		
		
		
		<div id="cover">
			<div class="blurfoto" style="background: url('/images/cover1.jpg')no-repeat center center;"></div>
			<div class="blurcolor"></div>
			<div class="rrss">
				<a href="https://www.facebook.com/TequilaChinacoPremium" target="_tab"><div class="ico"><i class="fa fa-facebook-f" aria-hidden="true"></i></div></a>
				<a href="https://www.instagram.com/chinaco_tequila/"  target="_tab"><div class="ico"><i class="fa fa-instagram" aria-hidden="true"></i></div></a>
			</div>
			<img src="/images/50years.svg" id="years">
		</div>
		
		
		<main>
			
			<section id="excepcional">
				<div class="centrador">
					
					<div class="color">
						<div class="centro">
							<h3>Tequila</h3>
							<h4>Excepcional</h4>
							<div class="features">
								<label>1972</label>
								<label>G.G.D.L.</label>
							</div>
							<p>{{ __('Tequila Chinaco tiene más de 50 años de tradición.') }}
{{ __('Es el único producido en el estado mexicano de Tamaulipas y es el primer Tequila de Alta Gama / Premium disponible en los Estados Unidos desde 1983 cambiando para siempre la historia del Tequila en América.') }}</p>
						</div>
					</div>
					
					<div class="photo">
						<div class="blurfoto" style="background: url('/images/excepcional.jpg')no-repeat center center;"></div>
					</div>
					
				</div>
			</section>
			
			
			<section id="productos">
				<div class="centrador">
					
					<div class="color">
						<div class="centro">
							<h4>{{ __('Nuestros') }}</h4>
							<h3>{{ __('Productos') }}</h3>
							<p>{{ __('La Gonzaleña, casa de familia, se remonta a las tierras del General Manuel González, Presidente de México 1880-1884 y héroe de la Batalla del Cinco de Mayo. La familia del general tiene el honor de continuar produciendo la firma de Chinaco en sus cuatro expresiones Blanco, Reposado, Añejo y Extra Añejo, siempre bajo el estandarte de excelencia en calidad y tradición.') }} 
{{ __('En Chinaco hacemos de la producción un trabajo de orgullo que da como resultado un tequila superior como ningún otro.') }}</p>
						</div>
					</div>
					
				</div>
			</section>
			
			
			<section id="products">
				<div class="centrador">
					
					<div class="producto flip craft" id="product1">
						<div class="color">
							<div class="centro">
								<h4>Tequila</h4>
								<h3>Blanco</h3>
								<div class="features">
									<label>40 {{ __('GRADOS') }}</label>
									<label>700ML.</label>
								</div>
								<p>{{ __('Chinaco Blanco se distingue por su sabor fresco 100% de agave. Embotellado en los cinco días posteriores a su destilación, cuenta con un sabor notablemente fresco y puro, perfecto para tomar a sorbos, o para preparar margaritas y cocteles exquisitos.') }}
{{ __('Chinaco Blanco es cristalino y presenta sabores a ramo de pera, membrillo, eneldo y lima, matizados con aloe. Se desliza de manera suave y brillante sobre el paladar dejando percibir profundidad y equilibrio excepcionales, seguidos de un final suave, largo y persistente.') }}</p>
							</div>
						</div>
						<div class="photo">
							<div class="blurfoto" style="background: url('/images/products/product1.jpg')no-repeat center center;"></div>
							<img src="/images/gold.png">
						</div>
					</div>
					
					
					
					<div class="producto @if(App::isLocale('en')) craft @endif" id="product2">
						<div class="color">
							<div class="centro">
								<h4>Tequila</h4>
								<h3>Reposado</h3>
								<div class="features">
									<label>40 {{ __('GRADOS') }}</label>
									<label>700ML.</label>
								</div>
								<p>{{ __('Chinaco Reposado se añeja once meses en barricas de roble blanco provenientes de Francia e Inglaterra previamente utilizadas para añejar whisky escocés y que hoy día cuentan con 35 años de antigüedad, confiriéndole un carácter suave y amaderado. Este tequila se mezcla maravillosamente aunque también se destaca por sí mismo.') }}
{{ __('De color ámbar claro, Chinaco Reposado expide un generoso aroma a cáscara de cítricos, melocotón y manzana, con toques de eneldo y membrillo. Los sabores son limpios y se transmiten al paladar con toda la profundidad y equilibrio característicos, coronados por un final medio a largo, voluptuoso, afrutado y especiado.') }}</p>
							</div>
						</div>
						<div class="photo">
							<div class="blurfoto" style="background: url('/images/products/product2.jpg')no-repeat center center;"></div>
							<img src="/images/craft.png">
						</div>
					</div>
					
					
					<div class="producto flip craft" id="product3">
						<div class="color">
							<div class="centro">
								<h4>Tequila</h4>
								<h3>Añejo</h3>
								<div class="features">
									<label>40 {{ __('GRADOS') }}</label>
									<label>700ML.</label>
								</div>
								<p>{{ __('Chinaco Añejo se deja madurar durante 30 meses en una mezcla de barricas similares a la de Chinaco Reposado combinadas con algunos barriles de Bourbon americano, lo que garantiza su constante suavidad y carácter. El resultado es complejo, notablemente suave y a la vez abundante, el cual debe saborearse como un buen coñac. De color ámbar dorado, Chinaco Añejo, ofrece aromas a pera, flores silvestres, vainilla, humo, manzana asada y bordes de papaya y mango. Presume de sabores  opulentos, pero equilibrados, aportando un estilo excepcional terminando en un exquisito final especiado y ahumado.') }}</p>
							</div>
						</div>
						<div class="photo">
							<div class="blurfoto" style="background: url('/images/products/product3.jpg')no-repeat center center;"></div>
							<img src="/images/craft.png">
						</div>
					</div>
					
					<div class="producto" id="product4">
						<div class="color">
							<div class="centro">
								<h4>Tequila</h4>
								<h3>Añejo</h3>
								<h6>50th Anniversary<br>Limited Edition</h6>
								<div class="features">
									<label>42 {{ __('GRADOS') }}</label>
									<label>750ML.</label>
								</div>
								<p>{{ __('Edición Especial Limitada, con solo 600 botellas producidas para México y U.S.A.') }}

{{ __('El Lote 89 es el último Tequila elaborado por nuestro fundador, Don Guillermo González Díaz Lombardo, a mediados de la década de los noventa. Este magnífico Tequila fue elaborado con los mejores agaves de la finca familiar y posteriormente añejado en sus barricas favoritas. Para luego ser almacenado en envases de vidrio por más de 20 años para una conmemoración especial.') }}

{{ __('Esta Edición Limitada es una “mezcla” de un tercio del Lote 89 y dos tercios del mejor Añejo que hemos producido en las últimas dos décadas, con los mismos orígenes y procesos que estableció nuestro fundador hace más de 50 años.') }}

{{ __('Dando como resultado un Añejo Excepcional!') }}</p>
							</div>
						</div>
						<div class="photo">
							<div class="blurfoto" style="background: url('/images/products/product4.jpg')no-repeat center center;"></div>
							<img src="/images/craft.png">
						</div>
					</div>
					
					
					@if(App::isLocale('es'))
					<div class="producto flip" id="product5">
						<div class="color">
							<div class="centro">
								<h4>Tequila</h4>
								<h3>Extra Añejo</h3>
								<div class="features">
									<label>40 - 43 GRADOS</label>
									<label>700ML.</label>
								</div>
								<p>Solo el 40 % de las barricas destinadas a la elaboración de Extra Añejo son merecedoras de la distinción de ser embotelladas para esta etiqueta. Cada una de estas fue seleccionada especialmente por la Maestra Destiladora, Doña Esther Hernández (q.e.p.d.), quien se aseguró de que Extra Añejo exhiba el carácter y los sabores de la pulpa de agave madura, horneada a la perfección, imprimiendo los sabores más finos e intensos disponibles para pasar por los procesos de  fermentación, destilación y añejamiento.</p>
							</div>
						</div>
						<div class="photo">
							<div class="blurfoto" style="background: url('/images/products/product5.jpg')no-repeat center center;"></div>
							<img src="/images/craft.png">
						</div>
					</div>
					@endif
					
					
					
				</div>
			</section>
			
			
			
			<section id="origen">
				<div class="centrador">
					
					<div class="color">
					
						<div class="blurfoto" style="background: url('/images/grabado.png')no-repeat center center;"></div>
						
						<div class="centro">
							<h4>{{ __('Nuestro') }}</h4>
							<h3>{{ __('Origen') }}</h3>
							
							<label>{{ __('Legado del') }}<br>{{ __('espíritu guerrero') }}</label>
							
							<p>{{ __('Los Chinacos fueron terratenientes independientes durante los siglos XVIII y XIX quienes, por necesidad, se convirtieron en legendarios guerreros. Defendieron incansablemente a México tanto en la Guerra de Reforma como durante la Intervención Francesa. Vitoreados por el pueblo, temidos por sus adversarios y reconocidos por su valentía y hazañas heroicas, fueron liderados por el General Manuel González, guerrero, estratega y caballero destacado por su inspirador coraje y liderazgo.') }} 
		
		{{ __('La historia comienza en 1856 cuando el General “Manco” González, como lo llamó el pueblo, haciendo referencia a su legendaria hazaña de continuar peleando aún después de haber perdido el brazo, adquiere tierras en su natal Tamaulipas, donde un largo tiempo después, en 1972, el fundador Guillermo González Díaz Lombardo construyó nuestra casa, Tequilera La Gonzaleña.') }}</p>
						
						</div>
					
					
					</div>
					
					
				</div>
			</section>
			
			
			<Heritage></Heritage>
			
			
			<section id="terroir">

						
				<div class="blurfoto" id="desktop" style="background: url('/images/mexico.png')no-repeat center center;"></div>
				<div class="blurfoto" id="mobile" style="background: url('/images/mexico2.png')no-repeat center bottom;"></div>
				<div class="blurcolor"></div>
				
				<div id="locations">
					<div class="location" id="tamaulipas">
						<img src="/images/pointer_1.svg">
						<b>Tamaulipas</b>
						<span>Tequila<br>Chinaco</span>
					</div>
					<div class="location" id="jalisco">
						<img src="/images/pointer_2.svg">
						<b>Jalisco</b>
						<span>{{ __('Mayoría de') }}<br>Tequilas</span>
					</div>
				</div>
				
				<div class="centrador">
						
					<div class="centro">
						<h4>Chinaco</h4>
						<h3>Terroir</h3>
						<label>Tamaulipas</label>
						<p>{{ __('El tequila es una bebida espirituosa con Denominación de Origen y solamente puede ser producido legalmente en algunos municipios de cinco estados de la República Mexicana: Jalisco, Nayarit, Guanajuato, Michoacán y Tamaulipas (solo Chinaco).') }} 
	
	{{ __('Cada territorio influye en los perfiles de sabor y textura del tequila, convirtiéndolo en uno de los licores más complejos del mundo.') }}
	
	{{ __('Practicando la sustentabilidad, anualmente, Tequilera La Gonzaleña, cultiva alrededor de 60, 000 plantas en el rancho familiar, El Rosillo. Esto, junto con el abastecimiento aportado por otros agricultores locales, garantiza el suministro de agave orgánico necesario para la producción del Tequila Chinaco; es una práctica inédita en la actualidad donde las plantas jóvenes proceden de las propias plantas madre que se cultivan en la misma región, asegurando la consistencia y calidad inigualables, sello excepcional de Tequila Chinaco.') }}</p>
					</div>
							
				</div>

			</section>
			
			
			<section id="sabor">
				<div class="centrador">
					
					<div id="photo">
						<div class="blurfoto" style="background: url('/images/tequilera.jpg')no-repeat center center;"></div>
					</div>
					
					<div id="factors">
						<div class="factor center">
							<div class="texto">
								<span>{{ __('Los sabores de tequilas artesanales son afectados por una alta variedad de factores:') }}</span>
							</div>
						</div>
						<div class="factor left">
							<div class="dot"></div>
							<div class="texto">
								<h4>{{ __('La') }}<br>{{ __('Región') }}</h4>
								<span>{{ __('El tipo de suelo, la geografía incluyendo elevación y exposición del sol. Así como el microclima.') }}</span>
							</div>
						</div>
						<div class="factor right">
							<div class="dot"></div>
							<div class="texto">
								<h4>{{ __('El') }}<br>Agave</h4>
								<span>{{ __('El origen, el cultivo, la edad y la madurez.') }}</span>
							</div>
						</div>
						<div class="factor left">
							<div class="dot"></div>
							<div class="texto">
								<h4>{{ __('El') }}<br>{{ __('Proceso') }}</h4>
								<span>{{ __('La extracción del azúcar del agave, la fermentación, la destilación y el añejamiento.') }}</span>
							</div>
						</div>
					</div>
					
				</div>
			</section>
			
			
			
			<section id="reconocimientos">
				<div class="centrador">
					
					<div id="slider">
						<div id="photos">
							<div class="photo" :class="{active: slider==1}">
								<div class="blurfoto" style="background: url('/images/slider1.jpg')no-repeat center center;"></div>
								<div class="blurcolor"></div>
								<div class="centro active">
									<h4>{{ __('Presentado en') }}</h4>
									<span>"{{ __('LAS MEJORES MARCAS DE') }}
											{{ __('TEQUILA PARA BEBER AHORA" DE') }}
											{{ __('Jonah Flicker, EN LA REVISTA:') }}</span>
									<br>
									<img src="/images/esquire.svg">
									<span>{{ __('Julio') }} 2022</span>
								</div>
							</div>
							<div class="photo" :class="{active: slider==2}">
								<div class="blurfoto" id="desktop" style="background: url('/images/slider2.jpg')no-repeat center center;"></div>
								<div class="blurfoto" id="mobile" style="background: url('/images/slider2b.jpg')no-repeat center center;"></div>
								<div class="blurcolor"></div>
								<div class="centro active">
									<h3>“No. 1 </h3>
									<span>of 10 most
									Influential spirits
									Brands of the
									Past 25 years.”</span>
									<br>
									<small>-WINE & SPIRITS MAGAZINE</small>
								</div>
							</div>
						</div>
						<nav id="prev" @click="prev">
							<div class="arrow">
								<img src="/images/prev.svg">
							</div>
						</nav>
						<nav id="next" @click="next">
							<div class="arrow">
								<img src="/images/next.svg">
							</div>
						</nav>
					</div>
					
				</div>
			</section>
			
			
			
			@if(App::isLocale('en'))
			<section id="ubicaciones">
				<div id="grappos-locator" style="width:100%; height:625px;"></div>
			</section>
			@endif
			
			
			
		
			<Contacto></Contacto>
					
					
			
			
			
		</main>
		
		
		@include('components.footer')
		
		
	</div>


@endsection

