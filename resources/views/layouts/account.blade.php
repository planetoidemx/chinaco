@extends('layouts.default')

@section('title','Planetoide.mx')

@section('metas')

   	<meta property="og:url" content="{{ Request::url() }}"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Planetoide.mx"/>
	<meta property="og:image" content="{{ asset('images/meta_img.jpg') }}"/>
	<meta property="og:description" content=""/>
	<meta property="fb:app_id" content=""/>

@endsection


@section('content')

    <div id="account">

      

        <main>
	        
	        <div id="nav">
		        <div class="centrador">
			        
			        <nav>
				        <div class="link"><span>Mi cuenta</span></div>
				        <img src="/images/svg/">
				        
				        @yield('account_link')
				        
			        </nav>
			        
		        </div>
	        </div>
	        

	        <section id="cuenta">
		        <div class="centrador">

			        <aside>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link"><span>Mi cuenta</span><img src="/images/svg/"></div>
				        <div class="link" v-on:click="requestViaForm('/logout', 'POST')">Cerrar sesión</div>
			        </aside>

			        <div id="main">
						
						@yield('account_content')
	
						@foreach ($customer->orders as $order)
							<a href="{{ $order->link }}" target="_bank">
								<div class="orden">
								{{$order->reference }}
								{{ $order->created_at->setTimeZone($timeZone)->toFormattedDateString() }}
								</div>
							</a>
						@endforeach

			        </div>

		        </div>

	        </section>



        </main>


	</div>

@endsection
