@extends('admin.website.items')


{{-- Seccion View --}}
@section('preview')

<div id="website_preview">

</div>

@endsection
{{-- Seccion View --}}


{{-- Seccion de Form --}}
@section('form')

<div class="formularios" id="website_form">

    <form {{-- action="{{ route('models.update', compact('model')) }}" --}} method="POST" enctype="multipart/form-data" ref="formEdit">
        @csrf
        @method('PUT')


        <button type="button" @click="saveWithSpinner($refs.formEdit)">Terminé</button>

    </form>

</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActionDelete', 'hidePreview']"></panel-config>

<panel-live-view-item live-route="{{-- {{ route('site.', compact('model')) }} --}}"></panel-live-view-item>

@endsection
{{-- Seccion de Form --}}

@push('scripts')

<script>

</script>

@endpush
