@extends('admin.dashboard')

@section('title', 'Admin | Discounts')

@section('bar_title', 'Discounts')

{{-- Seccion de items --}}
@section('items')

<a href="{{ route('discounts.create') }}">
    <div class="item discount_item new">
        <label>New</label>
    </div>
</a>

<a href="{{ route('discounts.edit') }}">
    <div class="item discount_item off highlight">

        <div class="foto"></div>

        <div class="texto">
            <strong></strong>
            <span></span>
        </div>

    </div>
</a>

{{-- $discounts->links() --}}

<panel-config :actions="['hideFilters', 'hideSearch']"></panel-config>

@endsection

{{-- Seccion de items --}}
