@extends('admin.discounts.items')

{{-- Seccion de Form --}}
@section('form')

<div class="formularios" id="discount_form">

	<form @submit.prevent="saveWithSpinner" {{-- action="{{ route('.', compact('model')) }}" --}} method="POST" enctype="multipart/form-data" ref="formName">
		@csrf



		<button type="button" @click="saveWithSpinner($refs.formName)">Terminé</button>

	</form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActions', 'hidePreview']"></panel-config>

@endsection
{{-- Seccion de Form --}}
