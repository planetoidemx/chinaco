@extends('admin.discounts.items')


{{-- Seccion View --}}

@section('preview')

<div id="discount_preview">

</div>

@endsection

{{-- Seccion View --}}





{{-- Seccion de Form --}}

@section('form')

<div class="formularios" id="discount_form">

	<form @submit.prevent="saveWithSpinner" {{-- action="{{ route('.', compact('model')) }}" --}} method="POST" enctype="multipart/form-data" ref="formName">
		@csrf
		@method('PUT')



		<button type="button" @click="saveWithSpinner($refs.formName)">Terminé</button>

	</form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActionDelete', 'hideActionShow', 'hideActions', 'hidePreview']"></panel-config>

<panel-delete-item message="Delete this discount?" delete-route="{{-- {{ route('models.destroy', compact('model')) }} --}}"></panel-delete-item>

<panel-live-view-item live-route="{{-- {{ route('site.', compact('model')) }} --}}"></panel-live-view-item>

@endsection
{{-- Seccion de Form --}}

@push('scripts')


@endpush
