@extends('admin.blog.items')


{{-- Seccion View --}}
@section('preview')

<div id="blog_preview">

</div>

@endsection
{{-- Seccion View --}}


{{-- Seccion de Form --}}
@section('form')

<div class="formularios" id="blog_form">

	<div class="seccion">
		<h6>{{ $post->title }}</h6>
		<div class="content">

			<div v-if="editing">
				<form action="{{ route('posts.update', compact('post')) }}" method="POST" enctype="multipart/form-data" ref="formEdit">
					@csrf
					@method('PUT')


					<div class="fotos" id="image">
						<div class="foto" :style="imgBackground('{{ $post->mainImage }}')">
							<input type="file" name="main_image" @change="setImage" accept="image/*">
							<div class="type">
								<i class="fa fa-image" aria-hidden="true"></i>
								<label>Post image</label>
							</div>
						</div>
					</div>

					<div class="fotos" id="cover">
						<div class="foto" :style="imgBackground('{{ $post->cover }}')">
							<input type="file" name="cover" @change="setImage" accept="image/*">
							<div class="type">
								<i class="fa fa-image" aria-hidden="true"></i>
								<label>Cover</label>
							</div>
						</div>
					</div>


					<div class="input">
						<label>Title</label>
						<input type="text" name="title" value="{{ old('title', $post->title) }}" placeholder="Title">
						<div class="focus"></div>
					</div>

					<div class="input">
						<label>Author</label>
						<input type="text" name="author" value="{{ old('author', $post->author) }}" placeholder="Author">
						<div class="focus"></div>
					</div>


					<div class="input">
						<label>Type</label>
						<select name="type" >
							<option value="post" @if($post->type == 'post') selected @endif>Post</option>
							<option value="event" @if($post->type == 'event') selected @endif>Event</option>
						</select>
						<div class="focus"></div>
					</div>

					<div class="input">
						<label>Source link</label>
						<input type="text" name="source_link" value="{{ old('source_link', $post->source_link) }}" placeholder="Source link">
						<div class="focus"></div>
					</div>


					<div class="input">
						<label>Show to</label>
						<select name="visibility">
							<option value="everyone" @if($post->isVisibleFor('everyone')) selected @endif>Everyone</option>
							<option value="members" @if($post->isVisibleFor('members')) selected @endif>Members</option>
							<option value="no_one" @if($post->isVisibleFor('no_one')) selected @endif>No one</option>
						</select>
						<div class="focus"></div>
					</div>


					<button type="button" @click="saveWithSpinner($refs.formEdit)">Done</button>

				</form>
			</div>

			<button type="button" v-if="!editing" @click="editing = true">Update post info</button>

		</div>
	</div>


	<div class="seccion">
		<h6>Contents</h6>
		<div class="content">

			<blocks :routes="{{ json_encode($post->blockRoutes()) }}" :post-id="{{ $post->id }}"></blocks>

		</div>
	</div>



</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hidePreview']"></panel-config>

<panel-delete-item message="Delete this post?" delete-route="{{ route('posts.destroy', compact('post')) }}"></panel-delete-item>

<panel-live-view-item live-route="{{ route('site.post', compact('post')) }}"></panel-live-view-item>

@endsection
{{-- Seccion de Form --}}


