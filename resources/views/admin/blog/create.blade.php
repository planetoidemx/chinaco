@extends('admin.blog.items')

{{-- Seccion de Form --}}
@section('form')

<div class="formularios white" id="blog_form">

	<form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data" ref="formCreate">
		@csrf

		<div class="fotos" id="image">
			<div class="foto">
				<input type="file" name="main_image" @change="setImage" accept="image/*">
				<div class="type">
					<i class="fa fa-image" aria-hidden="true"></i>
					<label>Post image</label>
				</div>
			</div>
		</div>

		<div class="fotos" id="cover">
			<div class="foto">
				<input type="file" name="cover" @change="setImage" accept="image/*">
				<div class="type">
					<i class="fa fa-image" aria-hidden="true"></i>
					<label>Cover</label>
				</div>
			</div>
		</div>


		<div class="input">
			<label>Title</label>
			<input type="text" name="title" value="{{ old('title') }}" placeholder="Title">
			<div class="focus"></div>
		</div>

		<div class="input">
			<label>Author</label>
			<input type="text" name="author" value="{{ old('author') }}" placeholder="Author">
			<div class="focus"></div>
		</div>

		<div class="input">
			<label>Type</label>
			<select name="type">
				<option value="post">Post</option>
				<option value="event">Event</option>
			</select>
			<div class="focus"></div>
		</div>


		<div class="input">
			<label>Source link</label>
			<input type="text" name="source_link" value="{{ old('source_link') }}" placeholder="Source link">
			<div class="focus"></div>
		</div>


		<div class="input">
			<label>Show to</label>
			<select name="visibility">
				<option value="everyone">Everyone</option>
				<option value="no_one">No one</option>
			</select>
			<div class="focus"></div>
		</div>


		<button type="button" @click="saveWithSpinner($refs.formCreate)">Done</button>

	</form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActions', 'hidePreview']"></panel-config>

@endsection
{{-- Seccion de Form --}}
