@extends('admin.dashboard')

@section('title', 'Admin | Blog')

@section('bar_title', 'Blog')

{{-- Seccion de items --}}
@section('items')

<a href="{{ route('posts.create') }}">
	<div class="item blog_item new">
		<label>New</label>
	</div>
</a>

@foreach ($posts as $post)
	<a href="{{ route('posts.edit', compact('post')) }}">
		<div class="item blog_item {{ $post->isVisibleFor('no_one') ? 'off' : '' }}">

			<div class="foto">
				<img src="{{ $post->mainImage }}">
			</div>

			<div class="texto">
				<strong>{{ $post->title }}</strong>
				<span>{{ $post->created_at->format('d/m/Y') }}</span>
			</div>

		</div>
	</a>
@endforeach

{{ $posts->links() }}

<panel-config :actions="['hideFilters', 'hideSearch']"></panel-config>

@endsection

{{-- Seccion de items --}}
