@extends('admin.shippings.items')

{{-- Seccion de Form --}}
@section('form')

<div class="formularios white" id="shipping_form">

    <form action="{{ route('shippingOptions.store') }}" method="POST" enctype="multipart/form-data" ref="createForm">
        @csrf

        <div class="input">
            <label>Company</label>
            <input type="text" name="name" value="{{ old('name') }}" placeholder="Company">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Days</label>
            <input type="text" name="description" value="{{ old('description') }}" placeholder="Days">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Fee $</label>
            <input type="text" name="fee" value="{{ old('fee') }}" placeholder="Fee $">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Tracking URL</label>
            <input type="text" name="tracking_link" value="{{ old('tracking_link') }}" placeholder="Tracking URL">
            <div class="focus"></div>
        </div>

        <div class="input check">
			<input type="hidden" name="is_active" value="0">
            <input type="checkbox" name="is_active" value="1" checked>
            <div class="checkbox">
                <i class="fa fa-check" aria-hidden="true"></i>
            </div>
            <label>Active</label>
        </div>

        <button type="button" @click="saveWithSpinner($refs.createForm)">Done</button>

    </form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActions', 'hidePreview']"></panel-config>

@endsection
{{-- Seccion de Form --}}
