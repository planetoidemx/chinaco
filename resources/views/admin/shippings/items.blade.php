@extends('admin.dashboard')

@section('title', 'Admin | Shipping')

@section('bar_title', 'Shipping')

{{-- Seccion de items --}}
@section('items')

<a href="{{ route('shippingOptions.create') }}">
	<div class="item shipping_item new">
		<label>Add shipping option</label>
	</div>
</a>

@foreach ($shippingOptions as $shippingOption)

<a href="{{ route('shippingOptions.edit', compact('shippingOption')) }}">
	<div class="item shipping_item {{ $shippingOption->is_active ? '' : 'off' }}">

		<div class="texto">
			<strong>{{ $shippingOption->name }}</strong>
			<span>{{ $shippingOption->description }}</span>
		</div>

	</div>
</a>

@endforeach


{{ $shippingOptions->links() }}

<panel-config :actions="['hideFilters', 'hideSearch']"></panel-config>

@endsection

{{-- Seccion de items --}}
