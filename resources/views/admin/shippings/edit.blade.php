@extends('admin.shippings.items')


{{-- Seccion View --}}

@section('preview')

<div id="shipping_preview">

</div>

@endsection

{{-- Seccion View --}}


{{-- Seccion de Form --}}

@section('form')

<div class="formularios white" id="shipping_form">

    <form action="{{ route('shippingOptions.update', compact('shippingOption')) }}" method="POST" enctype="multipart/form-data" ref="updateForm">
        @csrf
        @method('PUT')

        <div class="input">
            <label>Company</label>
            <input type="text" name="name" value="{{ old('name', $shippingOption->name) }}" placeholder="Company">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Days</label>
            <input type="text" name="description" value="{{ old('description', $shippingOption->description) }}" placeholder="Days">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Fee $</label>
            <input type="text" name="fee" value="{{ old('fee', $shippingOption->fee) }}" placeholder="Fee $">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Tracking URL</label>
            <input type="text" name="tracking_link" value="{{ old('tracking_link', $shippingOption->tracking_link) }}" placeholder="Tracking URL">
            <div class="focus"></div>
        </div>

        <div class="input check">
			<input type="hidden" name="is_active" value="0">
			<input type="checkbox" name="is_active" value="1" @if($shippingOption->is_active) checked @endif>
            <div class="checkbox">
                <i class="fa fa-check" aria-hidden="true"></i>
            </div>
            <label>Active</label>
        </div>

        <button type="button" @click="saveWithSpinner($refs.updateForm)">Done</button>

    </form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActionShow', 'hidePreview']"></panel-config>

<panel-delete-item message="Are you sure you want to delete this option?" delete-route="{{ route('shippingOptions.destroy', compact('shippingOption')) }}"></panel-delete-item>


@endsection
{{-- Seccion de Form --}}
