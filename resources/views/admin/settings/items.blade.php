@extends('admin.dashboard')

@section('title', 'Admin | Settings')

@section('bar_title', 'Settings')

{{-- Seccion de items --}}
@section('items')


<a href="{{ route('settings.shipping', compact('shippingSetting')) }}">
	<div class="item setting_item">

		<div class="texto">
			<strong>Free Shipping</strong>
		</div>

	</div>
</a>

<panel-config :actions="['hideFilters', 'hideSearch']"></panel-config>

@endsection

{{-- Seccion de items --}}
