@extends('admin.settings.items')


{{-- Seccion View --}}

@section('preview')

<div id="setting_preview">

</div>

@endsection

{{-- Seccion View --}}





{{-- Seccion de Form --}}

@section('form')

<div class="formularios" id="setting_form">


    <div class="seccion">
        <h6>Free Shipping</h6>
        <div class="content">

            <form action="{{ route('settings.update', ['setting' => $shippingSetting]) }}" method="POST" ref="shippingUpdateForm">
                @csrf
                @method('PUT')

                <div class="input placeholder">
                    <label>Min amount</label>
                    <input type="number" placeholder="$0" value="{{ $shippingSetting->value('min_amount') }}" name="values[min_amount]">
                    <div class="focus"></div>
                </div>

                <div class="input check">
                    <input type="hidden" name="values[is_active]" value="0">
                    <input type="checkbox" name="values[is_active]" value="1" @if($shippingSetting->boolean('is_active')) checked @endif>
                    <div class="checkbox">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <label>Active</label>
                </div>

                <button type="button" @click="saveWithSpinner($refs.shippingUpdateForm)">Done</button>

            </form>

        </div>
    </div>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActionDelete', 'hideActionShow', 'hideActions', 'hidePreview']"></panel-config>

@endsection
{{-- Seccion de Form --}}
