@extends('admin.dashboard')

@section('title', 'Admin | Orders')

@section('bar_title', 'Orders')

@section('search_route', 'orders.index')

{{-- Seccion de items --}}
@section('items')


@foreach ($orders as $order)
<a href="{{ route('orders.edit', compact('order')) }}">
    <div class="item order_item">

        <div class="texto">
            <strong>{{ $order.customer.email }}</strong>
            <span>{{ $order->created_at->format('d/m/Y') }}</span>
        </div>

        <order-status status="{{ $order->status }}"></order-status>

    </div>
</a>
@endforeach


{{ $orders->links() }}

<panel-config :actions="['hideFilters']"></panel-config>

@endsection

{{-- Seccion de items --}}
