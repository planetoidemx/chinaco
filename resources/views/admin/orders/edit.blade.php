@extends('admin.orders.items')


{{-- Seccion View --}}

@section('preview')

<div id="order_preview">



    <div class="seccion" id="products">
        <h6>Products</h6>
        <div class="content">

            <div id="productos">

                @foreach ($order->products as $product)

                <div class="producto">
                    <div class="foto" :style="imgBackground('{{ $product->pivot->variant('cover') ?? $product->cover }}')">
                    </div>
                    <div class="cantidad">
                        <span>x{{ $product->pivot->quantity }}</span>
                    </div>
                    <div class="nombre">
                        <span>
                            {{ $product->title }}{{ $product->pivot->hasVariant() ? ' - '.$product->pivot->variant('title') : '' }}
                            {{ $product->pivot->tagsAsString() }}
                        </span>
                        <p>{{ $product->pivot->notes }}</p>

                    </div>
                    <div class="precio">
                        <b>@money($product->pivot->charged_price)</b>
                    </div>
                </div>


                @endforeach


            </div>

            <div class="content">

                <div class="contenido">

                    <div class="info">
                        <label>Subtotal</label>
                        <p>@money($order->subtotal)</p>
                    </div>

                    <div class="info">
                        <label>Shipping</label>
                        <p>@money($order->shipping_fee)</p>
                    </div>

                    @if ($order->discount > 0)
                    <div class="info">
                        <label>Discount</label>
                        <p>-@money($order->discount)</p>
                    </div>
                    @endif

                    <div class="info">
                        <label>Total</label>
                        <p>@money($order->amount)</p>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <div class="seccion" id="customer">
        <h6>Customer contact information</h6>
        <div class="content">

            <div class="contenido">
                <div class="info">
                    <label>Full Name</label>
                    <p>{{ $order->customer->full_name }}</p>
                </div>
                <div class="info">
                    <label>Email</label>
                    <p>{{ $order->customer->email }}</p>
                </div>
                <div class="info">
                    <label>Phone number</label>
                    <p>{{ $order->customer->phone }}</p>
                </div>
            </div>
        </div>

    </div>

    <div class="seccion" id="shipping">
        <h6>Shipping information</h6>
        <div class="content">

            <div class="contenido">

                <div class="info">
                    <label>Street Address</label>
                    <p>{{ $order->customer->address }}</p>
                </div>
                <div class="info">
                    <label>City</label>
                    <p>{{ $order->customer->city }}</p>
                </div>
                <div class="info">
                    <label>Country</label>
                    <p>{{ $order->customer->country }}</p>
                </div>
                <div class="info">
                    <label>Postal Code</label>
                    <p>{{ $order->customer->postal_code }}</p>
                </div>
                <div class="info">
                    <label>Fee</label>
                    <p>@money($order->shipping_fee)</p>
                </div>

                @if (filled($order->customer->references))
                <div class="info">
                    <label>Delivery instructions</label>
                    <p>{{ $order->customer->references }}</p>
                </div>
                @endif


                @if (filled($order->track_number))
                <div class="info">
                    <label>Carrier</label>
                    <p>{{ $order->shippingOption->name }}</p>
                </div>

                <div class="info">
                    <label>Tracking number</label>
                    <p>{{ $order->track_number }}</p>
                </div>
                @endif


            </div>

        </div>
    </div>

    <div class="seccion" id="payment">
        <h6>{{ $order->hasPaymentError() ? $order->payment_error_message : 'Payment' }}</h6>
        <div class="content">

            <div class="contenido">
                <div class="info">
                    <label>Method</label>
                    <p>{{ $order->paidWithCard() ? 'Credit/Debit Card' : 'PayPal' }}</p>
                </div>

                @if ($order->paidWithCard() && !$order->hasPaymentError())

                <div class="info">
                    <label>Cardholder's name</label>
                    <p>{{ $order->payment_holders_name }}</p>
                </div>
                <div class="info">
                    <label>Card</label>
                    <p>{{ $order->card_brand }} - {{ $order->card_last4 }}</p>
                </div>

                @endif

                @if ($order->paidWithPayPal() && !$order->hasPaymentError())

                <div class="info">
                    <label>Paypal account</label>
                    <p>{{ $order->payment_holders_name }}</p>
                </div>

                @endif

                @if($order->hasPaymentError())
                <div class="info">
                    <label>Issue</label>
                    <p>{{ $order->error_message }}</p>
                </div>
                @else
                <div class="info">
                    <label>Reference</label>
                    <p>{{ $order->payment_reference }}</p>
                </div>
                @endif

                <div class="info">
                    <label>{{ $order->hasPaymentError() ? 'Pending amount' : 'Amount' }}</label>
                    <p>@money($order->amount)</p>
                </div>

                @if (filled($order->receipt_url) && !$order->hasPaymentError())
                <div class="info">
                    <label>Receipt link</label>
                    <p>
                        <a href="{{ $order->receipt_url }}" target="_tab">
                            <u style="text-decoration: underline;">Stripe Receipt</u>
                        </a>
                    </p>
                </div>
                @endif
            </div>

        </div>
    </div>


</div>

@endsection

{{-- Seccion View --}}





{{-- Seccion de Form --}}

@section('form')

<div class="formularios" id="order_form">


    <div class="seccion">
        <h6>Order ID: <b>#{{ $order->reference }}</b></h6>
        <div class="content">

            <form action="{{ route('orders.update', compact('order')) }}" method="POST" enctype="multipart/form-data" ref="updateForm">
                @csrf
                @method('PUT')

                <!-- add class: paid, complete o error -->
                <div class="input {{ $order->hasPaymentError() ? 'error' : $order->status }}" id="status">
                    <label>Status</label>
                    <input type="text" value="{{ $order->payment_error_message }}" placeholder="Status">
                    <div class="focus"></div>
                </div>

                <div class="input">
                    <label class="stay">Change to</label>
                    <select name="status">
                        <option selected disabled>Choose a status</option>
                        <option value="complete">Complete</option>
                        <option value="paid">Paid</option>
                    </select>
                    <div class="focus"></div>
                </div>

                <div class="input">
                    <label>Chossen option</label>
                    <input type="text" value="{{ $order->shippingOption->name. '/'. $order->shippingOption->description }} - @money($order->shippingOption->fee)" placeholder="Shipping option" readonly>
                    <div class="focus"></div>
                </div>

                <div class="input">
                    <label>Tracking ID</label>
                    <input type="text" name="track_number" value="{{ old('track_number', $order->track_number) }}" placeholder="Tracking ID">
                    <div class="focus"></div>
                </div>

                <button type="button" @click="saveWithSpinner($refs.updateForm)">Done</button>

            </form>

        </div>
    </div>



    <form id="delete_orders_form" action="{{ route('orders.destroy', compact('order')) }}" method="post">
        @csrf
        @method('DELETE')
    </form>

</div>

<panel-change-view view="preview"></panel-change-view>

<panel-config :actions="['hideActionShow']"></panel-config>

<panel-delete-item message="Delete this order? (#{{ $collection->reference }})" delete-route="{{ route('orders.destroy', compact('order')) }}"></panel-delete-item>

@endsection
{{-- Seccion de Form --}}
