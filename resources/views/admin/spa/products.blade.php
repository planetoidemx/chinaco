@extends('admin.dashboard')

@section('title', "Admin | Products")

@section('bar_title', 'Products')

@section('search_route', 'products.index')

@section('items')

<product-items :routes="{{ json_encode($routes) }}" item-class="product"></product-items>

@endsection



@section('form')

<product-form :routes="{{ json_encode($routes) }}" item-class="product"></product-form>

@endsection



@section('preview')

{{-- <product-preview :routes="{{ json_encode($routes) }}" item-class="product"></product-preview> --}}

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideFilters', 'hidePreview']"></panel-config>

@endsection

{{-- Seccion de items --}}

@push('scripts')
<script>

</script>
@endpush
