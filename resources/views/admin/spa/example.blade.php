@extends('admin.dashboard')

@section('title', "Admin | Example")

@section('bar_title', 'Example')

@section('items')

<example-items :routes="{{ json_encode($routes) }}" item-class="promo"></example-items>

@endsection



@section('form')

<example-form :routes="{{ json_encode($routes) }}" item-class="promo"></example-form>

@endsection



@section('preview')

{{-- <example-preview :routes="{{ json_encode($routes) }}" item-class="promo"></example-preview> --}}

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideFilters', 'hideSearch', 'hidePreview']"></panel-config>

@endsection

{{-- Seccion de items --}}

@push('scripts')
<script>

</script>
@endpush
