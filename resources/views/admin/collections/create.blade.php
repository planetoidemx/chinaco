@extends('admin.collections.items')

{{-- Seccion de Form --}}
@section('form')

<div class="formularios white" id="collection_form">

    <form action="{{ route('collections.store') }}" method="POST" enctype="multipart/form-data" ref="createForm">
        @csrf

        <div class="fotos" id="collection_cover">
            <div class="foto">
                <input type="file" name="cover" @change="setImage" accept="image/*">
                <div class="type">
                    <i class="fa fa-image" aria-hidden="true"></i>
                    <label>Collection Cover</label>
                </div>
            </div>
        </div>

        <div class="input">
            <label>Collection</label>
            <input type="text" name="title" value="{{ old('title') }}" placeholder="Collection">
            <div class="focus"></div>
        </div>

        <div class="input">
            <label>Description</label>
            <textarea name="description" placeholder="Description" rows="8">{{ old('description') }}</textarea>
            <div class="focus"></div>
        </div>

        <button type="button" @click="saveWithSpinner($refs.createForm)">Done</button>

    </form>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hideActions', 'hidePreview']"></panel-config>

@endsection
{{-- Seccion de Form --}}
