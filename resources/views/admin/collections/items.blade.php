@extends('admin.dashboard')

@section('title', 'Admin | Collections')

@section('bar_title', 'Collections')

{{-- Seccion de items --}}
@section('items')

<a href="{{ route('collections.create') }}">
    <div class="item collection_item new">
        <label>Add collection</label>
    </div>
</a>


@foreach ($collections as $collection)
<a href="{{ route('collections.edit', compact('collection')) }}">
    <div class="item collection_item">

        <div class="foto" :style="imgBackground('{{ $collection->cover }}')"></div>

        <div class="texto">
            <strong>{{ $collection->title }}</strong>
        </div>

    </div>
</a>
@endforeach

{{ $collections->links() }}

<panel-config :actions="['hideFilters', 'hideSearch']"></panel-config>

@endsection

{{-- Seccion de items --}}
