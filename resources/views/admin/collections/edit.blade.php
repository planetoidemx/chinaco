@extends('admin.collections.items')


{{-- Seccion View --}}

@section('preview')

<div id="collection_preview">

</div>

@endsection

{{-- Seccion View --}}





{{-- Seccion de Form --}}

@section('form')

<div class="formularios" id="collection_form">



    <div class="seccion">
        <h6>{{ $collection->title }}</h6>
        <div class="content">

            <div v-if="editing">

                <form action="{{ route('collections.update', compact('collection')) }}" method="POST" enctype="multipart/form-data" ref="updateForm">
                    @csrf
                    @method('PUT')

                    <div class="fotos" id="collection_cover">
                        <div class="foto" :style="imgBackground('{{ $collection->cover }}')">
                            <input type="file" name="cover" @change="setImage" accept="image/*">
                            <div class="type">
                                <i class="fa fa-image" aria-hidden="true"></i>
                                <label>Collection Cover</label>
                            </div>
                        </div>
                    </div>

                    <div class="input">
                        <label>Collection</label>
                        <input type="text" name="title" value="{{ old('title', $collection->title) }}" placeholder="Collection">
                        <div class="focus"></div>
                    </div>

                    <div class="input">
                        <label>Description</label>
                        <textarea name="description" placeholder="Description" rows="8">{{ old('description', $collection->description) }}</textarea>
                        <div class="focus"></div>
                    </div>

                    <button type="button" @click="saveWithSpinner($refs.updateForm)">Done</button>

                </form>

            </div>

            <button type="button" v-if="!editing" @click="editing = true">Edit collection info</button>

            <form id="delete_lines_form" action="{{ route('collections.destroy', compact('collection')) }}" method="post">
                @csrf
                @method('DELETE')
            </form>

        </div>



    </div>

    <div class="seccion" id="categories">
        <h6>Categories</h6>
        <div class="content">

            <blocks-categories :routes="{{ json_encode([
					'index' => route('categories.index', ['type' => 'collection', 'model' => $collection]),
				]) }}"
				:collection-id="{{ $collection->id }}">
            </blocks-categories>

        </div>
    </div>


</div>

<panel-change-view view="edit_view"></panel-change-view>

<panel-config :actions="['hidePreview']"></panel-config>

<panel-delete-item message="Delete this collection? ({{ $collection->title }})" delete-route="{{ route('collections.destroy', compact('collection')) }}"></panel-delete-item>

<panel-live-view-item live-route="{{ route('shop.index', compact('collection')) }}"></panel-live-view-item>

@endsection
{{-- Seccion de Form --}}
