import { createStore } from 'vuex';
import createPersistedState from 'vuex-persistedstate';

export const store = createStore({
	strict: process.env.NODE_ENV !== 'production',
	plugins: [createPersistedState()],

	state: {
		askedage: false,
		legalage: false,
		choosedlang: false,
	},
	getters: {
		legalage: (state) => {
			return state.legalage;	
		},
		choosedlang: (state) => {
			return state.choosedlang;	
		},
		askedage: (state) => {
			return state.askedage;	
		},
	},

	mutations: {
		choosedlang(state, choosedlang){
			state.choosedlang = choosedlang;	
		},
		legalage(state, legalage){
			state.legalage = legalage;	
		},
		askedage(state, askedage){
			state.askedage = askedage;	
		},
	},

	actions: {
		
		confirmlegalage({commit}){
			commit('legalage', true);
			commit('askedage', true);
		},
		
		confirmchoosedlang({commit}){
			commit('choosedlang', true);
		},
		
		confirmaskedage({commit}){
			commit('askedage', true);
		},
		
		askagain({commit}){
			commit('askedage', false);
		},
		
		underage({commit}){
			commit('legalage', false);
			commit('askedage', true);
		},
	},
});
