/**
 * Vue app for site views
 */
// import router from '@/router';
import { createApp, defineAsyncComponent } from 'vue';
import {mapGetters,mapActions} from 'vuex';

const app = createApp({
	components: {
		// ExampleComponent: defineAsyncComponent(() => import('@/lazycomponents/site/ExampleComponent')),
		//App: defineAsyncComponent(() => import('@/lazycomponents/site/App')),
		planetoide: defineAsyncComponent(() => import('@/lazycomponents/site/Planetoide')),
		ShowNotifications: defineAsyncComponent(() => import('@/lazycomponents/ShowNotifications')),
		Error: defineAsyncComponent(() => import('@/lazycomponents/Error')),
		SetLocale: defineAsyncComponent(() => import('@/lazycomponents/site/SetLocale')),
		Contacto: defineAsyncComponent(() => import('@/lazycomponents/site/Contacto')),
	},
	// store,
	// router,
	// template: '<App/>',
	data() {
		return {
			locale: null,
			showPasswords: false,
			slider: 1,
			time:6000,
			timer: null,
		};
	},
	methods: {
		...mapActions(['confirmlegalage','confirmchoosedlang','confirmaskedage','askagain','underage']),	
		setLocale(locale) {
			this.locale = locale;
		},
		prev(){
			if(this.slider==1){
				this.slider=2;
			}else{
				this.slider=1;
			}
		},
		next(){
			if(this.slider==1){
				this.slider=2;
			}else{
				this.slider=1;
			}
		},
		
		start(){
			
			this.timer = setInterval(()=>{
				
				if(this.slider==1){
					this.slider=2;
				}else{
					this.slider=1;
				}
				
			},this.time);
			
		},
		
		setlang(lang){
			this.confirmchoosedlang();
			setTimeout(()=>{
				this.redirect(route('site.localization', {locale: lang} ));
			},600);
		},
		
	},
	computed: {
		...mapGetters(['legalage','choosedlang','askedage']),	
	},
	mounted(){
		
		setTimeout(()=>{
			if(!this.ageverified){
				//this.age=true;
			}
		}, 500);
		
		if(window.innerWidth < 820){
			this.start();
		}
			
	},
});

/**
 * Register Asteroide site plugins
 *
 */

import useInstance from '@/plugins/instance';
import useSite from '@/plugins/site';
import useShop from '@/plugins/shop';
import VueScrollTo from 'vue-scrollto';
import { ZiggyVue } from 'ziggy';
import { Ziggy } from './ziggy';
import { store } from '@/store';

app.use(ZiggyVue, Ziggy);
app.use(useInstance);
app.use(useSite);
app.use(useShop);
app.use(VueScrollTo);
app.use(store);

/**
 * Autoload site global compoments
 *
 * Eg. ./components/site/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components/site', true, /\.vue$/i);

files.keys().map((key) => app.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Mount the App
 */
app.mount('#app');
