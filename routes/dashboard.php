<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
|
*/

use App\Http\Controllers\Dashboard\ActivityController;
use App\Http\Controllers\Dashboard\CollectionController;
use App\Http\Controllers\Dashboard\LoginController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\PostController;
use App\Http\Controllers\Dashboard\PromoController;
use App\Http\Controllers\Dashboard\SettingsController;
use App\Http\Controllers\Dashboard\ShippingOptionController;
use App\Http\Controllers\Dashboard\SpaController;
use App\Http\Controllers\Dashboard\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->group(function () {
    // Nueva syntaxis ---> Route::get('user/profile', [UserProfileController::class, 'show'])->name('profile');

    // RUTAS SIMPLES PARA VIEWS EJEMPLO
    // Route::view('users', 'admin.users.items')->name('users.index');
    // Route::view('users/create', 'admin.users.create')->name('users.create');
    // Route::view('users/edit', 'admin.users.edit')->name('users.edit');

    // Authentication
    Route::controller(LoginController::class)->group(function () {
        Route::get('/', 'showLoginForm')->name('admin.login');
        Route::post('/', 'login')->name('admin.login.make');
        Route::post('logout', 'logout')->name('admin.logout');
    });

    Route::middleware('auth')->group(function () {
        Route::middleware('role:super_admin')->group(function () {
            // Monitor
            Route::resource('monitor', ActivityController::class)
                ->parameters(['monitor' => 'activity'])
                ->names('monitor')
                ->only(['index', 'show']);
        });

        // Users
        Route::resource('users', UserController::class)->except(['show']);

        // Promos
        Route::resource('promos', PromoController::class)->except(['show']);

        // Collections
        Route::resource('collections', CollectionController::class)->except(['show']);

        // Shipping Options
        Route::resource('shippings', ShippingOptionController::class)
            ->parameters(['shippings' => 'shippingOption'])
            ->names('shippingOptions')
            ->except(['show']);

        // Discounts
        Route::view('discounts', 'admin.discounts.items')->name('discounts.index');
        Route::view('discounts/create', 'admin.discounts.create')->name('discounts.create');
        Route::view('discounts/edit', 'admin.discounts.edit')->name('discounts.edit');

        Route::view('website', 'admin.website.items')->name('website.index');
        Route::view('website/home', 'admin.website.home')->name('website.home');

        // Need role --> super_admin|admin
        Route::middleware('role:super_admin,admin')->group(function () {
            // Settings
            Route::controller(SettingsController::class)->group(function () {
                Route::get('settings', 'index')->name('settings.index');
                Route::put('settings/{setting}', 'update')->name('settings.update');
                Route::get('settings/{shippingSetting}/free-shipping', 'shipping')->name('settings.shipping');
            });
        });

        // Orders
        Route::resource('orders', OrderController::class)->only(['index', 'edit', 'update', 'destroy']);

        Route::controller(SpaController::class)
            ->prefix('spa')->group(function () {
                Route::get('products', 'products')->name('spa.products');
            });

        // Blog
        Route::resource('blog', PostController::class)
            ->parameters(['blog' => 'post'])
            ->names('posts')
            ->except('show');
    });
});
