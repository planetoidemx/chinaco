<?php

use App\Http\Controllers\CustomerSessionController;
use App\Http\Controllers\Dashboard\BlockController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\FormDataController;
use App\Http\Controllers\Dashboard\ImageController;
use App\Http\Controllers\Dashboard\MasiveController;
use App\Http\Controllers\Dashboard\ProductController;
use App\Http\Controllers\Dashboard\PropController;
use App\Http\Controllers\Dashboard\PropGroupController;
use App\Http\Controllers\Dashboard\TagController;
use App\Http\Controllers\Dashboard\TagGroupController;
use App\Http\Controllers\Dashboard\VariantController;
use App\Http\Controllers\SiteApiController;
use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

// Auth required routes
Route::prefix('admin')->group(function () {
    Route::middleware(['auth:api'])->group(function () {
        // Collections
        // Route::get('collection/{collection}/blocks', [BlockController::class, 'indexForCollection'])->name('blocks_collections.index');
        // Route::post('collection/{collection}/blocks', [BlockController::class, 'storeForCollection'])->name('blocks_collections.store');

        // Blog blocks and contents
        Route::post('blocks-index', [BlockController::class, 'index'])->name('blocks.index');
        Route::apiResource('blocks', BlockController::class)->except('index', 'show');

        // Products
        Route::apiResource('products', ProductController::class);

        // Images
        Route::post('images-index', [ImageController::class, 'index'])->name('images.index');
        Route::apiResource('images', ImageController::class)->only('store', 'destroy');

        // Variants
        Route::controller(MasiveController::class)->group(function () {
            Route::post('masive/product/{product}/props', 'storeProps')->name('masive.props');
            Route::post('masive/product/{product}/variants', 'storeVariants')->name('masive.variants');
            Route::post('masive/product/{product}/tags', 'storeTags')->name('masive.tags');
        });

        // PropGroup & props
        Route::apiResource('product/{product}/propGroup', PropGroupController::class)->except(['show']);
        Route::apiResource('propGroup/{propGroup}/props', PropController::class)->only(['store', 'update',  'destroy']);

        // Variants
        Route::apiResource('product/{product}/variants', VariantController::class)->only(['index', 'update', 'destroy']);

        // PropGroup & props
        Route::apiResource('product/{product}/tagGroup', TagGroupController::class)->except(['show']);
        Route::apiResource('tagGroup/{tagGroup}/tags', TagController::class)->only(['store', 'update',  'destroy']);

        // Categories
        Route::post('category-index', [CategoryController::class, 'index'])->name('categories.index');
        Route::apiResource('categories', CategoryController::class)->except(['index', 'show']);

        Route::controller(FormDataController::class)->group(function () {
            // Data routes
            Route::get('data/collections', 'collections')->name('data.collections');

            // LiveView links routes
            Route::get('links/product/{product}', 'productLink')->name('links.product');
        });
    });
});

// Public routes
Route::prefix('public')->group(function () {
    Route::controller(FormDataController::class)->group(function () {
        Route::get('shippingoptions', 'shippingOptions')->name('public.shippingOptions');
        Route::post('summary/options', 'sendFreeShippingCondition')->name('summary.options');
    });

    Route::controller(SiteApiController::class)->group(function () {
        Route::get('products/collection/{collection?}', 'collectionProducts')->name('collection.products');
        Route::get('products/category/{category}', 'categoryProducts')->name('category.products');
        Route::post('subscribe/newletter', 'subscribe')->name('subscriptions.newsletter');

        Route::prefix('product/{product}')->group(function () {
            Route::get('/', 'product')->name('public.product');
            Route::get('props', 'props')->name('public.product.props');
            Route::get('variant/prop1/{prop1}', 'variant')->name('public.variant.simple');
            Route::get('variant/prop1/{prop1}/prop2/{prop2}', 'variant')->name('public.variant.double');
        });
    });
});

Route::middleware('auth:account')
    ->controller(CustomerSessionController::class)
    ->group(function () {
        Route::get('profile/{customer}', 'show')->name('profile.show');
        Route::put('profile/{customer}', 'updateProfile')->name('profile.update');
    });

Route::post('webhooks', [WebhookController::class, 'handler']);
