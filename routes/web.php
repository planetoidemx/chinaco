<?php

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
|
*/

use App\Http\Controllers\CustomerSessionController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::name('site.')->group(function () {
    // Nueva syntaxis ---> Route::get('user/profile', [UserProfileController::class, 'show'])->name('profile');

    // RUTAS SIMPLES PARA VIEWS EJEMPLO
    // Route::view('/', 'home')->name('home');

    Route::get('/', [SiteController::class, 'home'])->name('home');
    Route::get('localization/{locale}', [LocalizationController::class, 'index'])->name('localization');
    Route::get('blog', [SiteController::class, 'blog'])->name('blog');
    Route::get('blog/{post:slug}', [SiteController::class, 'post'])->name('post');
});

Route::name('shop.')->group(function () {
    // eShop Routes

    Route::get('shop/{collection:slug?}', [SiteController::class, 'shop'])->name('index');
    Route::get('shop/{collection:slug}/{product:slug}/{any?}', [SiteController::class, 'productOfCollection'])->name('product.of.collection')->where('any', '.*');
    Route::get('shop/category/{category:slug}/{product}/{any?}', [SiteController::class, 'productOfCategory'])->name('product.of.category')->where('any', '.*');
    Route::view('cart', 'shop.cart')->name('cart');
    Route::view('checkout', 'shop.checkout')->name('checkout');
    Route::match(['get', 'post'], 'checkout/confirmation', [PaymentController::class, 'pay'])->name('process');
    Route::get('checkout/order/{order}', [PaymentController::class, 'order'])->name('order');

    Route::middleware('auth.at:shop')->group(function () {
        Route::get('account', [CustomerSessionController::class, 'account'])->name('account');
    });
});

/*
|--------------------------------------------------------------------------
| Vue routing support
|--------------------------------------------------------------------------
|
*/

// Route::any('/{any}', 'SiteController@index')->where('any', '.*');
